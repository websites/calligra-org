---
menu:
  main:
    name: Calligra
    weight: 1
    params:
      components:
      - name: Words
        url: /components/words
        background: 'https://apps.kde.org/app-icons/org.kde.calligra.words.svg'
      - name: Stage
        url: /components/stage
        background: 'https://apps.kde.org/app-icons/org.kde.calligra.stage.svg'
      - name: Sheets
        url: /components/sheets
        background: 'https://apps.kde.org/app-icons/org.kde.calligra.sheets.svg'
      - name: Karbon
        url: /components/karbon
        background: 'https://apps.kde.org/app-icons/org.kde.calligra.karbon.svg'
      - name: Kexi
        url: /components/kexi
        background: 'https://apps.kde.org/app-icons/org.kde.kexi-3.3.svg'
      - name: Plan
        url: /components/plan
        background: 'https://apps.kde.org/app-icons/org.kde.calligraplan.svg'
      - name: Old Components
        url: /components/old-components
        background: 'https://apps.kde.org/app-icons/org.kde.calligra.svg'
title: Calligra Suite
---

Calligra Suite is an office and graphic art suite by KDE.  It contains
applications for word processing, spreadsheets, presentation, vector graphics,
and editing databases.
