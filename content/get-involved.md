---
title: Get Involved
name: Calligra
userbase: Calligra
menu:
  main:
    weight: 4
layout: get-involved
getintouch: |
  Calligra Suite is a collection of free software application, made by a community of volunteers, with different skills, contributing to different areas.
  
  No matter what your skills are, there is something you can do to help with the project. If you want to get in touch with the community, you can join the IRC channel [#calligra](irc://irc.libera.chat/calligra) on Libera Chat, join the mailing-list [calligra-devel@kde.org](https://mail.kde.org/mailman/listinfo/calligra-devel), use our [community page](https://community.kde.org/Calligra), use the [forum](https://discuss.kde.org), or finally [KDE’s bugs tracker](https://bugs.kde.org/).
---
