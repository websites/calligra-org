---
title: Gemini
image: /assets/img/gemini-summary.png
subtitle: >
  Calligra for 2 in 1 devices
description: >
  Calligra Gemini is an open source office suite updated with a transforming interface for 2 in 1 devices, is a cool step in office application design.
order: 10
draft: true
aliases:
 - ../gemini
---

Calligra Gemini is an open source office suite updated with a transforming interface for 2 in 1 devices, is a cool step in office application design

Imagine making last minute changes to your presentation, then handing your customer the 2 in 1 device in tablet mode so your presentation plays before them with touchscreen interaction. Or imagine being able to easily edit a text document when you’re using your 2 in 1 device in tablet mode. The transformable user interface (UI) added to these applications—Calligra Stage for presentations and Calligra Words for word processing—will make this kind of usage a reality.
