---
title: Plan
image: /assets/img/plan-summary.png
subtitle: Effective Project Planning
description: >
    Plan is a project management application. It is intended for managing moderately large projects with multiple resources.To enable you to model your project adequately, Plan offers different types of task dependencies and timing constraints. The usual use case is to define your tasks, estimate the effort needed to perform each task, allocate resources and then let Plan schedule the tasks according to network and resource availability.
weight: 5
aliases:
 - ../plan
---

Plan is a project management application. It is intended for managing moderately large projects with multiple resources.

To enable you to model your project adequately, Plan offers different types of task dependencies and timing constraints. The usual use case is to define your tasks, estimate the effort needed to perform each task, allocate resources and then let Plan schedule the tasks according to network and resource availability.

In case of changes, the project can be rescheduled, retaining original schedules for comparison. Especially useful is the ability to reschedule from the current state of the project. This can typically be used when the execution of the project does not go according to plan, resource availability changes or tasks have to be added to or removed from the project.
<br/>
<br/>

[![Project scheduling and analyzes](/assets/img/plan-schedule.png)](/assets/img/plan-schedule.png "Project scheduling and analyzes")

Task progress can be entered in different detail dependent on the need for progress monitoring, from a simple percent completed to the more detailed time used per resource and estimate of remaining effort.
Detailed progress report enables earned value and performance index calculation.

[![Task progress information](/assets/img/plan-progress.png)](/assets/img/plan-progress.png "Task progress information")


The user interface is optimized for keyboard operation and minimizes use of dialogs. Views and editors can be configured for optimal operation.

Tasks can be organized into a work breakdown structure with configurable WBS code.

Resources can reside in different time zones, and is organized into a resource breakdown structure.

[![Resource breakdown structure](/assets/img/plan-resource.png)](/assets/img/plan-resource.png "Resource breakdown structure")

Cost may be associated with task(s) and can be assigned to accounts organized into a cost breakdown structure.



