---
title: KEXI
subtitle: >
  Database creation for everyone
description: >
  KEXI is a visual database applications creator. It can be used for designing database applications, inserting and editing data, performing queries, and processing data. Forms can be created to provide a custom interface to your data. All database objects – tables, queries, forms, reports – are stored in the database, making it easy to share data and design.
image: /assets/img/kexi-summary.png
weight: 5
aliases:
 - ../kexi
---

KEXI is a visual database applications creator. It can be used for designing database applications, inserting and editing data, performing queries, and processing data. Forms can be created to provide a custom interface to your data. All database objects – tables, queries, forms, reports – are stored in the database, making it easy to share data and design.

KEXI is considered as a long awaited Open Source competitor for Microsoft Access, FileMaker and Oracle Forms. Its development is motivated by the lack of Rapid Application Development (RAD) tools for database systems that are sufficiently powerful, inexpensive, open standards driven and portable across many operating systems and hardware platforms.

KEXI is Free/Libre/Open-Source Software. As a real member of the KDE and Calligra projects, KEXI integrates fluently into both. It is designed to be fully usable while running outside of the KDE Plasma, so it can run on Linux/BSD/Unix e.g. under the GNOME desktop, on macOS, and on MS Windows.

## See also

- [KEXI home page](http://www.kexi-project.org)
- [KEXI forums](http://forum.kde.org/kexi)
- [KEXI Handbook](http://userbase.kde.org/Kexi/Handbook)
- [KEXI Info, Tutorials and Samples](http://userbase.kde.org/Kexi)

## Screenshots

[![KEXI- New Project](/assets/img/kexi-new.png)](/assets/img/kexi-new.png "KEXI- New Project")

<center> Startup dialog for a new Kexi project </center> <br/>

[![KEXI- Storing images](/assets/img/kexi-image.png)](/assets/img/kexi-image.png "KEXI- Storing images")

<center> KEXI Table View storing images </center> <br/>

[![KEXI-Form](/assets/img/kexi-form.png)](/assets/img/kexi-form.png "KEXI-Form")

<center>KEXI Form </center> <br/>

[![KEXI-Form Designer](/assets/img/kexi-form-design.png)](/assets/img/kexi-form-design.png "KEXI-Form Designer")

<center>KEXI Form Design View </center> <br/>

[![KEXI-Assign Actions](/assets/img/kexi-assign-action.png)](/assets/img/kexi-assign-action.png "KEXI-Assign Actions")

<center>KEXI - Assigning Actions </center> <br/>

[![Kexi-Report](/assets/img/kexi-report.png)](/assets/img/kexi-report.png "KEXI-Report")

<center>KEXI Report </center> <br/>

[![Kexi-Report Designer](/assets/img/kexi-report-design.png)](/assets/img/kexi-report-design.png "KEXI-Report Designer")

<center>KEXI Report Design View </center>

[![Kexi-Export to CSV](/assets/img/kexi-export.png)](/assets/img/kexi-export.png "KEXI-Export to CSV")

<center>KEXI Advanced CSV Export Dialog </center> <br/>

[![Kexi-SQL](/assets/img/kexi-sql.png)](/assets/img/kexi-sql.png "KEXI-SQL")

<center>KEXI SQL Support </center>