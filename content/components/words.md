---
title: Words
image: 'https://cdn.kde.org/screenshots/calligra/words-desktop-mode.png'
subtitle: >
    Rich documents with images, tables etc made easy
description: >
    Calligra Words is an intuitive word processor application with desktop publishing features. With it, you can create informative and attractive documents with ease. Calligra Words makes adding images, charts etc to your documents effortless. It’s as simple as dragging it onto the document.
weight: 1
aliases:
 - ../words
---

Calligra Words makes adding images, charts etc to your documents effortless. It’s as simple as dragging it onto the document.

Further editing is just as easy. For example if you move, rotate or change the size of an image the text will wrap round it seamlessly – just as it should.


## Compatibility

Calligra Words can open and save ODF documents as it’s default fileformat. The Open Document Format is a vendor neutral format supported by Calligra Suite, OpenOffice.org, LibreOffice, Microsoft Office, Google Docs, Lotus Symphony as well as many others.

Additionally it’s possible to open Microsoft Office Word files (both .doc and .docx). Afterwards you can edit and save the document to ODF.

## Screenshots

[![Print Preview dialog shows all pages](/assets/img/words-printpreview.png)](/assets/img/words-printpreview.png "Print Preview dialog shows all pages")

An easy and powerful print preview dialog allows you to save time and paper. When you are convinced the result is worth printing can be done to the printer of your choice or to a PDF file for electronic publishing.