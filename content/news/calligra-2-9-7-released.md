---
title: Calligra 2.9.7 Released
date: 2015-09-03
author: jstaniek
categories: []
---

The Calligra team is pleased to announce the release of [Calligra Suite, and Calligra Active 2.9.7](http://www.calligra.org). It is recommended update that brings further improvements to the 2.9 series of the applications and underlying development frameworks.

[Support Calligra!](https://www.calligra.org/news/calligra-2-9-7-released/#support)

## Bugfixes in This Release

Here is an overview of the most important fixes. There are several others that may be not mentioned here.

### General

- Removed a number of memory leaks in common code
- Properly set normal default paragraphstyle as parent to footnote/endnote default ones
- Fix copying text from inside a table cell without copying the entire cell (bug 350175)
- Optimalization of table cell formatting
- Fix: pressing Backspace in some cases didn't delete the selected table (bug 350426)
- Fix: Inserting a variable when having a selecion should overwrite the selection (bug 350435)
- Fix: Pasting into the before-table-paragraph breaks it (bug 350427)
- Make the final spell checking markup could be drawn the wrong way giving some weird visual glitches (bug 350433)
- Fix writing direction button not working the first time in some cases. Changed the way of detection the current direction. (bug 350432)
- Make icon size of the toolbox configurable (right-click on the toolbox to select a new size) (bug 336686)
- Add a couple smaller toolbox icon sizes (14 pixels)
- Make the default toolbox icons 14px since that looks closest to what they were before
- Update tool tips to include keyboard shortcut (tool tips will automatically change with changes to shorcuts) (bug 348626)
- Make the default size of the toolbox buttons dependent on screen resolution
- Create subfolders for presets (related bug 321361)
- Initialize colors to black, as per docs
- Improved memory usage (use vectors)
- Set the full file name as default directory in file dialogs

### Kexi

- _General_:
    - Fix vertical alignment of text in command link button widgets, it was especially broken in Breeze widget style (bug 349169)
    

- _Tables:_
    - Restore ability of altering table design. **This was serious regression** present in Kexi 2.9.5 and 2.9.6. (bug 350457)

- _Queries:_
    - Don't force saving when switching never stored query to Data view (on 2nd try)

- _CSV Import:_
    - Fix detection of primary key column on CSV import (bug 351487)
    - Fix updates of primary key detection when value of 'Start at line' changes

- _SQLite databases_:
    - Better results and error reporting for prepared statements

### Krita

(See also [Krita.org](https://krita.org/item/krita-2-9-7-released/))

- _Highlights:_
    - As is traditional, our September release has the first Google Summer of Code results. Wolthera's Tangent Normal Brush engine has already been merged and provides:
        - Tangent Normal Brush Engine
        - Phong Bumpmap now accepts normal map input
        - Normalize filter
        - Tilt Cursor
    - We've got all-new icons!
    - You can configure the size of the icons used in the toolbox
    - Colorspacebrowser: if you want to know the nitty-gritty details about the colorspaces and profiles Krita offers, all information is now available in the new colorspace browser. Still under heavy polishing!
    - You can pick colors and use the fill tool everwhere in wrap-around mode

- _Other new features:_
    - Implement ‘Scalable smoothness’ feature for Stabilizer smoother
    - Update tooltips for toolbox icons
    - Right click to undo last path point
    - Update tooltips to include keyboard shortcut
    - Make the default size of the toolbox buttons dependent on screen resolution
    - Added ability to merge down Selection Masks
    - Improve loading of PSDs of any colour space big time; 16bit CMYK psd files can now be loaded
    - Add three shortcuts to fill with opacity
    - Implement loading for ZIP compressed PSD files
    - XCF: load group layers from XCF files v3 or higher
    - Allow ‘shift’-modifer after dragging an assistant handle to snap lines
    - Add snap-single checkbox under assistant snapping.
    - Update brushes with optimised versions.(Basic\_tip\_default.kpp, Basic\_tip\_soft.kpp, Basic\_wet.kpp, Block\_basic.kpp, Block\_bristles.kpp, Block\_tilt.kpp, Ink\_brush\_25.kpp, Ink\_gpen\_10.kpp, Ink\_gpen\_25.kpp)
    - Mathematically robust normal map combination blending mode
    - Slow down updates for randomized brushes.
    - added convert to shape for selections
    - Added Trim to Image Size action
    - Optimise dodge and burn filter
    - Multiple layers merge with layer styles on Ctrl+E. (1) “Merge selected layers” is now deprecated and you can use usual Ctrl+E to merge multiple selection, (2) Mass-merging of layers with layer styles works correctly now, (3) Merging of clone layers together with their sources will not break Krita now)
    - Make color to alpha work with 16f channel depths
    - Add new shortcuts (Scale Image to new size = CTRL+ALT+I, Resize Canvas = CTRL+ALT+C, Create Group, Layer = CTRL+G, Feather Selection = SHIFT+F6)

- _Bug fixes:_
    - Fix abr brush loading (bug 351599)
    - Remember the toolbar visibility state (bug 343615)
    - Do not let the wheel zoom if there are modifiers pressed (patch by var.spool.mail700@gmail.com. Thanks!) (bug 338839)
    - Fix active layer activation mask (bug 347500)
    - Remove misleading error message after saving fails
    - Prevent Krita from loading incomplete assistant (bug 350289)
    - Add ctrl-shift-s as default shortcut for save as (bug 350960)
    - Fix Bristle brush presets
    - Fix use normal map checkbox in phongbumpmap filter UI
    - Fix loading the system-set monitorprofile
    - Make cs-convert UI attempt to automatically determine when to uncheck optimise
    - Do not share textures when that’s not possible (related bug 351488)
    - Remove disabling of system profile checkbox
    - Update the display profile when moving screens (related bug 351488)
    - Update the display profile after changing the settings
    - Fix crash due to calling a virtual method from c-tor of KisToolPaint
    - Disable the layerbox if there’s no open image (bug 351664)
    - Correctly install the xcf import plugin on Windows (bug 345285)
    - Fix Fill with… (Opacity) actions
    - Make a transform tool work with Pass Through group layers (bug 351548)
    - Fix parsing XML with MSVC 2012
    - Make all the lines of paintop options look the same
    - Make sure a default KoColor is transparent (bug 351560)
    - Lots of memory leak fixes (pointers that weren’t deleted are now deleted)
    - Blacklist “photoshop”:DateCreated” when saving (bug 351497)
    - Only add shortcuts for Krita
    - Only ask for a profile for 16 bits png images, since there we assume linear by default, which is wrong for most png images
    - Don’t build the thumb creator on Windows or OSX
    - Work around encoding issues in kzip (bug 350498)
    - Better error message in PNG export (bug 348099)
    - Don’t rename resources
    - Also change the color selector when selecting a vector layer (bug 336693)
    - Remove old compatibility code (bug 349554)
    - Disable the opacity setting for the shape brush (bug 349571)
    - Initialize KoColor to black, as per apidox
    - Add some explanation to the recovery dialog (related bug 351411)
    - Load resources from subfolders (bug 321361)
    - Recreate a default bounds object on every KisMask::setImage() call (related bug 345619)
    - Fix a severe crash in Transformation Masks (bug 349819)
    - Add a barrier between sequentially undone commands with setIndex (bug 349819)
    - Fixed API of KisPNGConverter to not acces the entire KisImage
    - Check which color spaces PNG supports before passing the preview device to it (bug 351383)
    - Save CMYK JPEG’s correctly (bug 351298)
    - Do not crash saving 16 bit CMYK to JPEG (bug 351298)
    - Fix slowdown when activating “Isolate Layer” mode (bug 351195)
    - Fix loading of selection masks
    - Accept events so oxygen doesn’t get them (bug 337187)
    - Added optional flags to KisDocument::openUrl() and made “File Layer” not add its file to the recent files list (bug 345560)
    - Fix crash when activating Passthrough mode for a group with transparency mask (bug 351224)
    - Don’t truncate fractional brush sizes on eraser switch (patch by Alexey Elnatanov. Thanks!) (bug 347798)
    - Fix layout of the color options page (bug 351271)
    - Don’t add new layers to the group if it is locked
    - Transform invisible layers if they are part of the group
    - All Drag & Drop of masks (bug 345619)
    - Optimisize advanced color selector
    - Select the right list item in the fill layer dialog
    - Remove excessive qDebug statements (bug 349871)
    - Remove the non-working fast grid settings (bug 349514)
    - Make the luma inputboxes aware of locale (bug 344490)
    - Don’t crash if there isn’t a pattern (bug 348940)
    - Fix location of colon in color management settings (bug 350128)
    - Don’t hang when isolating a layer during a stroke (bug 351193)
    - Palette docker: Avoid showing a horizontal scrollbar (bug 349621)
    - Stamp and Clipboard brush fixes
    - Sort the dockers alphabetically
    - Add the toolbox to the docker menu (bug 349732)
    - Make it possible to R-select layers in a pass-through group (bug 351185)
    - Set a minimum width for the tool option popup (bug 350298)
    - Fix build on ARM (bug 351164)
    - Fixing pattern png loading on bundles
    - Don’t stop loading a bundle when a wrong manifest entry is found
    - Fix inherit alpha on fill layers (bug 349333)
    - Fix to resource md5 generation
    - Fix full-screen/canvas-only state confusion (patch by Alexey Elnatanov, Thanks!) (bug 348981)
    - Brush editor Stamp and Clipboard refactoring (bug 345195)
    - Don’t crash on closing krita if the filter manager is open (bug 351005)
    - Fix a memory leak in KisWeakSharedPtr
    - Re-enable antialias for selection tools (bug 350803)
    - Open the Krita Manual on F1 on all platforms (bug 347285)
    - Update all the action icons when the theme is changed
    - Workaround for Surface Pro 3 Eraser (bug 341899)
    - Fix an issue with mimetype detection
    - Fix a crash when PSD file type is not magic-recognized by the system (bug 350588)
    - Fix a hangup when pressing ‘v’ and ‘b’ in the brush tool simultaneously (bug 350280)
    - Fix crash in the line tool (bug 350280)
    - Fix crash when loading a transform mask with a non-affine transform (bug 350507)
    - Fixed Flatten Layer and Merge Down actions for layer with layer styles (bug 349479)

### Document filters

- Fix encoding of import filter source files for Applix\* files

## Try It Out

![Download small](/assets/img/download-70.png)

The source code of the release is available for download here: [calligra-2.9.7.tar.xz](http://download.kde.org/stable/calligra-2.9.7/calligra-2.9.7.tar.xz). Also [translations to many languages](http://download.kde.org/stable/calligra-2.9.7/calligra-l10n/) and [MD5 sums](http://download.kde.org/stable/calligra-2.9.7/MD5SUMS). Alternatively, you can [download binaries for many Linux distributions and for Windows](http://userbase.kde.org/Calligra/Download#Stable_Release) (users: feel free to update that page).

  

## What's Next and How to Help?

The next step after the [2.9 series](https://community.kde.org/Calligra/Schedules/2.9/Release_Plan) is Calligra 3.0 which will be based on new technologies. We expect it later in 2015.

You can meet us to share your thoughts or offer your support on [general Calligra forums](http://forum.kde.org/calligra) or dedicated to [Kexi](https://forum.kde.org/kexi) or [Krita](https://forum.kde.org/krita). Many improvements are only possible thanks to the fact that we're working together within the awesome community.

(Some Calligra apps need new [maintainers](https://community.kde.org/Calligra/Maintainers), you can become one, it's fun!)

## How and Why to Support Calligra?

Calligra apps may be totally free, but their development is costly. Power, hardware, office space, internet access, travelling for meetings - everything costs. Direct donation is the easiest and fastest way to efficiently support your favourite applications. Everyone, regardless of any degree of involvement can do so. You can choose to: ![Heart](/assets/img/heart-701.png)

**Support entire Calligra indirectly by donating to KDE**, the parent organization and community of Calligra: [http://www.kde.org/community/donations](http://www.kde.org/community/donations).

![Heart](/assets/img/heart-701.png)

**Support Krita directly by donating to the Krita Foundation**, to support Krita development in general or development of a specific feature: [https://krita.org/support-us/donations](https://krita.org/support-us/donations/).

![Heart](/assets/img/heart-701.png)

**Support Kexi directly by donating to its current BountySource fundraiser**, supporting development of a specific feature, or the team in general: [https://www.bountysource.com/teams/kexi](https://www.bountysource.com/teams/kexi).

## About the Calligra Suite

![](/assets/img/calligra-logo-transparent-for-light-600-300x200.png)

Calligra Suite is a graphic art and office suite developed by the KDE community. It is available for desktop PCs, tablet computers and smartphones. It contains applications for word processing, spreadsheets, presentation, databases, vector graphics and digital painting. For more information visit [calligra.org](http://www.calligra.org/).

  

## About KDE

![](/assets/img/klogo-official-lineart_simple_bw-128x128.png)

[KDE is an international technology team](https://www.kde.org/community/whatiskde/) that creates free and open source software for desktop and portable computing. Among KDE's products are a modern desktop system for Linux and UNIX platforms, comprehensive office productivity and groupware suites and hundreds of software titles in many categories including Internet, multimedia, entertainment, education, graphics and software development. KDE's software available in more than 60 languages on Linux, BSD, Solaris, Windows and Mac OS X.

<!-- h3 { padding-top:1em; } .button, .button:link, .button:visited { text-decoration:none; text-align:center; padding:11px 32px; border:solid 1px #004F72; -webkit-border-radius:10px; -moz-border-radius:10px; border-radius: 10px; font:18px Arial, Helvetica, sans-serif; font-weight:bold; color:#E5FFFF; background-color:#3BA4C7; background-image: -moz-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -webkit-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -o-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -ms-linear-gradient(top, #3BA4C7 0% ,#1982A5 100%); filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1982A5', endColorstr='#1982A5',GradientType=0 ); background-image: linear-gradient(top, #3BA4C7 0% ,#1982A5 100%); -webkit-box-shadow:0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; -moz-box-shadow: 0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; box-shadow:0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; } .button:hover{ padding:11px 32px; border:solid 1px #004F72; -webkit-border-radius:10px; -moz-border-radius:10px; border-radius: 10px; font:18px Arial, Helvetica, sans-serif; font-weight:bold; color:#E5FFFF; background-color:#3BA4C7; background-image: -moz-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -webkit-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -o-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -ms-linear-gradient(top, #3BA4C7 0% ,#1982A5 100%); filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1982A5', endColorstr='#1982A5',GradientType=0 ); background-image: linear-gradient(top, #3BA4C7 0% ,#1982A5 100%); -webkit-box-shadow:0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; -moz-box-shadow: 0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; box-shadow:0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; } .button:active{ padding:11px 32px; border:solid 1px #004F72; -webkit-border-radius:10px; -moz-border-radius:10px; border-radius: 10px; font:18px Arial, Helvetica, sans-serif; font-weight:bold; color:#E5FFFF; background-color:#3BA4C7; background-image: -moz-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -webkit-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -o-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -ms-linear-gradient(top, #3BA4C7 0% ,#1982A5 100%); filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1982A5', endColorstr='#1982A5',GradientType=0 ); background-image: linear-gradient(top, #3BA4C7 0% ,#1982A5 100%); -webkit-box-shadow:0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; -moz-box-shadow: 0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; box-shadow:0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; } <div></div> .button a,.button a:link, .button a:visited, .button a:hover, .button a:active { color:#E5FFFF; } -->
