---
title: Calligra 3.2.1
layout: post
date: 2020-05-14
---

We are happy to announce the release of Calligra 3.2.1.

## Fixes in This Release

```
* Sheets: Workarounf for Bug 421336 - Sheets crash (sometimes) when loading LO generated files
* Styels: Write correct path to manifest also when styles is not in top level dir
* Guard agains crash if view has no parent
* Include existing documentation for Sheets and Stage.
```

## Get it

Check your distribution's repositories to find and install the Calligra 3.2.1.
