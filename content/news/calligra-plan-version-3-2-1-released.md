---
title: Calligra Plan version 3.2.1 released
date: 2019-12-02
author: danders
categories: [announcements,stable]
---

We are pleased to announce the relase of Calligra Plan 3.2.1.

Tarball can be found here:

[http://download.kde.org/stable/calligra/3.2.1/calligraplan-3.2.1.tar.xz.mirrorlist](http://download.kde.org/stable/calligra/3.2.1/calligraplan-3.2.1.tar.xz.mirrorlist)

**The following bugs have been fixed:**

```
* Bug 414164 - View specific dockers not shown in Settings->Dockers
* Bug 414162 - Crash during save
* Bug 414253 - Actual effort editable in list view
* Bug 414257 - Task modules settings not undoable
* Bug 414204 - Task modules parameter listed multiple times
* Bug 414133 - Task modules directory duplicated in Project Settings
* Entries in the workpackage merge dialog shall not be editable

```
