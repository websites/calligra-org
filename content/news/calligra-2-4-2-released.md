---
title: Calligra 2.4.2 Released
date: 2012-05-30
author: Inge Wallin
categories: []
---

The Calligra team has released version 2.4.2, the second bugfix release of the [Calligra Suite](http://www.calligra.org/ "Home page of the Calligra Suite"), Calligra Mobile and Calligra Active. This release contains a number of important bug fixes to 2.4.1 and we recommend everybody to update as soon as possible.

## Bugfixes in This Release

Here is an overview of the most important fixes are in 2.4.2. There are several others that are not mentioned here:

###  General:

- Speed improvement on layouting large documents (BUG: 298216)
- Fix image effects 'grayscale' and 'mono' (BUG: 260434)
- Fix saving of patterns
- Fix painting artefacts after snapping has been used (BUG: 260418)
- Fix adding word to dictionary (BUG: 298794)
- Fix crashes when using the color set popup (BUG:298412)
- Fix compilation with CLANG compiler

### Stage:

- Use masterpage background correctly (BUG: 299639)

### Kexi:

- Fix crashes when saving query design
    

### Krita:

- Do not fill layers that cannot be edited (BUG: 300082)
    
- Fix loading of layer name from Gimp files having bad encoding (BUG: 298994)
    
- Fix crash on opening pdf with special characters in the path (BUG: 298611)
    

### Charts:

- All standard 2D chart types be loaded and displayed now.
- Saving of all chart types above work.
- All crashes that happened when switching between different chart types are gone.
- Load correct data series in case of stock charts

## Try It Out

The source code of this version is available for download here: [calligra-2.4.2.tar.bz2](http://download.kde.org/stable/calligra-2.4.2/calligra-2.4.2.tar.bz2). Also many [binary packages](http://userbase.kde.org/Calligra/Download) are available for various operating systems and distributions. This release is packaged for Windows by [KO GmbH,](http://www.kogmbh.com/ "KO GmbH homepage") and you can find it on their [download page](http://www.kogmbh.com/download.html). There is also a separate package or [Krita](http://www.krita.org/) only.

We would welcome volunteers who want to build the Calligra Suite on OSX.

## About Calligra

Calligra is part of the applications from the KDE community. See more information at the website [http://www.calligra.org/](http://www.calligra.org/).
