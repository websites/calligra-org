---
title: Calligra 2.4 Beta 6
date: 2012-01-11
author: Inge Wallin
categories: [announcements]
---

The Calligra team is proud to announce the sixth beta version of the [Calligra Suite](http://www.calligra.org/ "Home page of the Calligra Suite"). This is likely the second to last beta before the first release candidate.

News in This Release

There is now a hard feature freeze and no more new features are allowed into Calligra. Therefore it is difficult to write about any specific news in this release.

The focus of this release has been to **improve saving and roundtripping**. This means that a document should contain the same information after loading and saving it as it did before the cycle. This is the final focus area that needs to be addressed before the final release.

Other than saving, the usual amount of crashes and normal bugs are fixed. And also as usual the team is grateful for all bug reports in this and other areas. See also [detailed list of changes for Kexi](http://community.kde.org/Kexi/Releases/Kexi_2.4_Beta_6).

## Try It Out

The source code of this version is available for download here: [calligra-2.3.86.tar.bz2](http://download.kde.org/download.php?url=unstable/calligra-2.3.86/calligra-2.3.86.tar.bz2). Also many [binary packages](http://userbase.kde.org/Calligra/Download#Unstable) are available for various operating systems and distributions. This release is packaged for Windows by [KO GmbH,](http://www.kogmbh.com/ "KO GmbH homepage") and you can find it on their [download page](http://www.kogmbh.com/download.html).

We would welcome volunteers who want to build the Calligra Suite on OSX.

## About the Calligra Suite

The Calligra Suite is part of the applications from the KDE community. See more information at the website [http://www.calligra.org/](http://www.calligra.org/).
