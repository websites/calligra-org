---
title: Calligra 2.9.8 Released
date: 2015-10-09
author: jstaniek
categories: []
---

The Calligra team announce availability of the [Calligra Suite, and Calligra Active 2.9.8](http://www.calligra.org). It is recommended update that improves the 2.9 series of the applications and underlying development frameworks.

[Support Calligra!](https://www.calligra.org/news/calligra-2-9-8-released/#support)

## Bugfixes in This Release

Here is an overview of the most important fixes. There are several others that may be not mentioned here.

### General

- Make the maximum default toolbox button size 22px (bug 352110)
- Fix display of toolbox icons on CentOS
- Fix crash when typing a bogus color-set name in the color set dialog

### Kexi

- _General_:
    
    - Avoid repeated opening of the same object, possible for large data
    
      
    

- _Tables:_
    
    - Table view's combo box list: ensure that highlighted row is visible
    - Table view: update the highlighted row while scrolling the viewport
    
      
    

- _Queries:_
    
    - **New feature:** Add support for 22 typical scalar SQL functions (wish 352631):  
        abs(X), ceiling(X), char(X1,X2,...,XN), coalesce(X,Y,...), floor(X), greatest(X,Y,...) and its alias max(X,Y,...), hex(X), ifnull(X,Y), instr(X,Y), least(X,Y,...) and its alias min(X,Y,...), length(X), lower(X), ltrim(X), ltrim(X,Y), nullif(X,Y), random(), random(X,Y), replace(X,Y,Z), round(X), round(X,Y), rtrim(X), rtrim(X,Y), soundex(X), trim(X), trim(X,Y), unicode(X), upper(X).  
        - With minor exceptions these functions now work with SQLite, MySQL, PostgreSQL
        - This make Kexi unique environment supporting unified SQL
        - In many cases substitute functions are used to make the result work in a portable way
        - No extra i18n added; will be added in a KDb port
        - Implementation of the function parser supports:  
            - checking number of arguments
            - checking types of arguments, sometimes "any number" or "any text or BLOB"
            - checking compatibility of arguments in function
            - checking relation of value of arguments in function, e.g. value of the first argument smaller than second's
            - checking NULL arguments in function; then most often the result is NULL
            - argument overloading or optional arguments, e.g. round(X) and round(X,Y)
            - displaying of human-readable error messages when the above constraints are not met, for example "Three arguments required" or "Function does not accept any arguments"
            - easy extensibility using just a few lines of code.
    - **New feature:** Add support for hex literals, useful for opearting on BLOBs in queries: X'ABCD' (SQL) and 0xABCD (ODBC) formats
    - Optimize evaluating type of expressions (also for database migration)
    - Fix broken floating-point constants in Kexi SQL queries, e.g. "1.002" was interpreted as "1.2" (bug 352363)
    - After parsing numbers are now kept losslessly as byte arrays, what enables unlimited precision
    - Improve parsing/sending BLOB literals
    - Improve performance of the SQL scanner
    - Make parsing of argument lists in scalar SQL functions more efficient by reversing direction
    
      
    

- _MySQL databases_:
    
    - Set _client\_library\_version_ property for newer versions of MySQL
    
      
    

- _PostgreSQL databases_:
    
    - Return NULL value for columns of NULL type
    - Switch to "BYTEA HEX" escaping while generating native statements
    
      
    

### Krita

(we're sorry, details on [Krita.org](https://krita.org/item/krita-2-9-8))

### Calligra Flow

- Fix lack of update of Flow's canvas and page count when deleting the current page

### Calligra Stage

- Let Ctrl+Home/Ctrl+End go to top/bottom of text shape in stage. Much more expected than exiting edit-mode and going to the first or last page.
- Fix crash when loading a presentation created with LO Impress
- Don't leave temporary files around

## Try It Out

![Download small](/assets/img/download-70.png)

The source code of the release is available for download here: [calligra-2.9.8.tar.xz](http://download.kde.org/stable/calligra-2.9.8/calligra-2.9.8.tar.xz). Also [translations to many languages](http://download.kde.org/stable/calligra-2.9.8/calligra-l10n/) and [MD5 sums](http://download.kde.org/stable/calligra-2.9.8/MD5SUMS). Alternatively, you can [download binaries for many Linux distributions and for Windows](http://userbase.kde.org/Calligra/Download#Stable_Release) (users: feel free to update that page).

  

## What's Next and How to Help?

The next step after the [2.9 series](https://community.kde.org/Calligra/Schedules/2.9/Release_Plan) is Calligra 3.0 which will be based on new technologies. We expect it later in 2015.

You can meet us to share your thoughts or offer your support on [general Calligra forums](http://forum.kde.org/calligra) or dedicated to [Kexi](https://forum.kde.org/kexi) or [Krita](https://forum.kde.org/krita). Many improvements are only possible thanks to the fact that we're working together within the awesome community.

(Some Calligra apps need new [maintainers](https://community.kde.org/Calligra/Maintainers), you can become one, it's fun!)

## How and Why to Support Calligra?

Calligra apps may be totally free, but their development is costly. Power, hardware, office space, internet access, travelling for meetings - everything costs. Direct donation is the easiest and fastest way to efficiently support your favourite applications. Everyone, regardless of any degree of involvement can do so. You can choose to: ![Heart](/assets/img/heart-701.png)

**Support entire Calligra indirectly by donating to KDE**, the parent organization and community of Calligra: [http://www.kde.org/community/donations](http://www.kde.org/community/donations).

![Heart](/assets/img/heart-701.png)

**Support Krita directly by donating to the Krita Foundation**, to support Krita development in general or development of a specific feature: [https://krita.org/support-us/donations](https://krita.org/support-us/donations/).

![Heart](/assets/img/heart-701.png)

**Support Kexi directly by donating to its current BountySource fundraiser**, supporting development of a specific feature, or the team in general: [https://www.bountysource.com/teams/kexi](https://www.bountysource.com/teams/kexi).

## About the Calligra Suite

![](/assets/img/calligra-logo-transparent-for-light-600-300x200.png)

Calligra Suite is a graphic art and office suite developed by the KDE community. It is available for desktop PCs, tablet computers and smartphones. It contains applications for word processing, spreadsheets, presentation, databases, vector graphics and digital painting. For more information visit [calligra.org](http://www.calligra.org/).

  

## About KDE

![](/assets/img/klogo-official-lineart_simple_bw-128x128.png)

[KDE is an international technology team](https://www.kde.org/community/whatiskde/) that creates free and open source software for desktop and portable computing. Among KDE's products are a modern desktop system for Linux and UNIX platforms, comprehensive office productivity and groupware suites and hundreds of software titles in many categories including Internet, multimedia, entertainment, education, graphics and software development. KDE's software available in more than 60 languages on Linux, BSD, Solaris, Windows and Mac OS X.

<!-- h3 { padding-top:1em; } .button, .button:link, .button:visited { text-decoration:none; text-align:center; padding:11px 32px; border:solid 1px #004F72; -webkit-border-radius:10px; -moz-border-radius:10px; border-radius: 10px; font:18px Arial, Helvetica, sans-serif; font-weight:bold; color:#E5FFFF; background-color:#3BA4C7; background-image: -moz-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -webkit-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -o-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -ms-linear-gradient(top, #3BA4C7 0% ,#1982A5 100%); filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1982A5', endColorstr='#1982A5',GradientType=0 ); background-image: linear-gradient(top, #3BA4C7 0% ,#1982A5 100%); -webkit-box-shadow:0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; -moz-box-shadow: 0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; box-shadow:0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; } .button:hover{ padding:11px 32px; border:solid 1px #004F72; -webkit-border-radius:10px; -moz-border-radius:10px; border-radius: 10px; font:18px Arial, Helvetica, sans-serif; font-weight:bold; color:#E5FFFF; background-color:#3BA4C7; background-image: -moz-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -webkit-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -o-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -ms-linear-gradient(top, #3BA4C7 0% ,#1982A5 100%); filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1982A5', endColorstr='#1982A5',GradientType=0 ); background-image: linear-gradient(top, #3BA4C7 0% ,#1982A5 100%); -webkit-box-shadow:0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; -moz-box-shadow: 0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; box-shadow:0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; } .button:active{ padding:11px 32px; border:solid 1px #004F72; -webkit-border-radius:10px; -moz-border-radius:10px; border-radius: 10px; font:18px Arial, Helvetica, sans-serif; font-weight:bold; color:#E5FFFF; background-color:#3BA4C7; background-image: -moz-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -webkit-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -o-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -ms-linear-gradient(top, #3BA4C7 0% ,#1982A5 100%); filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1982A5', endColorstr='#1982A5',GradientType=0 ); background-image: linear-gradient(top, #3BA4C7 0% ,#1982A5 100%); -webkit-box-shadow:0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; -moz-box-shadow: 0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; box-shadow:0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; } <div></div> .button a,.button a:link, .button a:visited, .button a:hover, .button a:active { color:#E5FFFF; } -->
