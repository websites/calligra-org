---
title: Calligra Packages for Windows Released
date: 2011-12-21
author: Inge Wallin
categories: []
---

[KO GmbH](http://www.kogmbh.com/ "Home page of KO") has released the first version of the Calligra Suite for Windows. The package is labeled as "**highly experimental**" and "**not yet suitable for daily use**".  It is aimed at people who want to try to run Calligra applications on Windows and are willing to report bugs so that stable builds of can be packaged when Calligra 2.4 is released early next year.

Many of the bugs in the packages are due to dependencies that are not really part of the Calligra software itself, but which are necessary to run it. Some of these are never before tested on Windows and may need fixing before Calligra can deliver the full user experience.

Inge Wallin, marketing coordinator for Calligra, says: "We have long wanted to spread our user base to incorporate Windows users as well. Since the Calligra project does not release binaries itself, we are happy that KO has made the effort to adapt and build Calligra for Windows. We hope that many users will try this package and report back any bug they find."

The work was kindly sponsored by [Nlnet Foundation](http://nlnet.nl/ "Nlnet Foundation"), a dutch organization that is behind many projects around the Open Document Format.

You can find the packages at the [Download page of the KO website](http://www.kogmbh.com/download.html "Downloads from KO").

About KO GmbH: KO GmbH is an expert consulting and development company with services around office document applications in general, the Open Document Format in particular and conversions between office document formats. It is incorporated in Germany but employs developers all over Europe and the United States.
