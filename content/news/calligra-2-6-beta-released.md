---
title: Calligra 2.6 Beta Released
date: 2012-10-31
author: Inge Wallin
categories: []
---

The Calligra team is proud and pleased to announce the beta release of version 2.6 of the [Calligra Suite](http://www.calligra-suite.org/ "Home page of the Calligra Suite"). This means that the calligra/2.6 branch has been created and from now on Calligra 2.6 will only see bufixes but no new features. The final release of 2.6 is planned within a month.

## News in This Release

As expected, this release has a number of new features that were not ready in time for the alpha release. Here are some of the most important ones:

**Plan**, the project management application, has updated scheduling information, improved scheduling granularity, improved performance charts, improved project creation, improved usability in the report generator and many bug fixes.

In **Kexi**, the visual database creator, overwriting objects with the same name is now possible. See [community.kde.org](http://community.kde.org/Kexi/Releases/Kexi_2.6_Beta) for complete list of changes.

There are two **general improvements** in handling of the Open Document format: Calligra will now load and save 3D shapes and annotations. This means better interoperability with other office applications.

Finally **Calligra Active**, the tablet version of Calligra, has new preview for text documents, a new text search feature for all 3 document formats, support for translations, a simplified UI and a few bugfixes.

## Try It Out

The source code of the snapshot is available for download: [calligra-2.5.91.tar.bz2](http://download.kde.org/unstable/calligra-2.5.91/calligra-2.5.91.tar.bz2). Alternatively, you can [download binaries for many Linux distributions and windows](http://userbase.kde.org/Calligra/Download#Unstable_Release). We are very pleased that version 12.10 of Kubuntu will include Krita and Kexi by default.

## About the Calligra Suite

The Calligra Suite is part of the applications from the KDE community. See more information at the website [http://www.calligra.org/](http://www.calligra.org/).
