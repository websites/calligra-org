---
title: Calligra 4.0
date: 2024-08-27
author: Carl Schwan
---

Calligra is the office and graphics suite developed by KDE and is the successor
to KOffice. With some traditional parts like Kexi and Plan having an
independent release schedule, this release only contains the four following
components:

* Calligra Words: Word Processor
* Calligra Sheets: Spreadsheet Application
* Calligra Stage: Presentation Application
* Karbon: Vector Graphics Editor

The most significant updates are that Calligra has been fully transitioned to
Qt6 and KF6, along with a major overhaul of its user interface.

[Read more](https://carlschwan.eu/2024/08/27/calligra-office-4.0-is-out/)
