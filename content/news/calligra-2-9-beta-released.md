---
title: Calligra 2.9 Beta Released
date: 2014-12-14
author: jstaniek
categories: []
coverImage: okular-generator-2.9.png
---

We're pleased to present you the **first beta release in 2.9 series** of [Calligra Suite](http://www.calligra-suite.org/ "Home page of the Calligra Suite") for testing! We will focus on fixing issues including those that you'd [report](http://bugs.kde.org/). All thus to make the final release of 2.9 [expected](https://community.kde.org/Calligra/Schedules/2.9/Release_Plan#Stable_releases) in January 2015 as stable as possible!

[Support Calligra!](https://www.calligra.org/news/calligra-2-9-beta-released/#support)

When you update [many improvements and a few new features](https://www.calligra.org/news/calligra-2-9-beta-released/#whats_new) will be installed, mostly in Kexi and Krita as well as general ones. Finally in 2.9 a new app, Calligra Gemini, appears. Read below to see why it may be of interest to you.  

## New Features and Improvements in This Release

### New Integration: Displaying office documents in Okular

\[caption id="attachment\_4325" align="alignright" width="300"\][![Calligra document plugin for Okular](/assets/img/okular-generator-2.9-300x200.png)](http://www.calligra.org/wp-content/uploads/2014/12/okular-generator-2.9.png) Calligra document plugin for Okular showing a DOC file\[/caption\]**A new plugin for [Okular](https://okular.kde.org/)**, KDE's universal document viewer, enables Okular to use the _Calligra office engine_ for displaying documents in the formats OpenDocument Text (ODT), MS Word (DOC, DOCX) and WordPerfect (WPD). It supplements the existing plugin from Calligra that gives Okular ability to display OpenDocument Presentation (ODP) and MS Powerpoint (PPT, PPTX) formats.

The _Calligra office engine_ has been used for the default document viewers on the smartphones [Nokia N9](https://www.calligra.org/news/calligra-provides-the-engine-for-nokias-harmattan-office/) and [Jolla](http://jolla.com/jolla), the Android app [COffice](https://play.google.com/store/apps/details?id=org.kde.calligra.coffice), and other mobile editions of Calligra. So it makes sense to also use the Calligra office engine for the document reader from KDE, coming with a UI designed for document consumption for people who want to read, but not edit office documents.

### New application: Calligra Gemini

[![Text document edited on laptop computer](/assets/img/calligra-gemini-desktop-2.9-sm.png)](http://www.calligra.org/wp-content/uploads/2014/12/calligra-gemini-desktop-2.9.png)[![The same text document in tablet mode](/assets/img/calligra-gemini-touch-2.9-sm-300x200.png)](http://www.calligra.org/wp-content/uploads/2014/12/calligra-gemini-touch-2.9.png)

The same text document edited on laptop computer and in tablet mode

**Calligra Gemini** debuts in 2.9, a novel application encasing word processor and presentation Calligra components can function both as a traditional desktop application used with a mouse and keyboard, and transform into a touch friendly application on the go. This changes the experience to one suitable for all-touch devices without the inconvenience of having to switch to a separate application.

Read more about [story behind the app](https://dot.kde.org/2014/11/21/calligra-gemini-added-calligra-suite).

### Kexi - Visual Database Applications Builder

Many usability improvements and bug fixes. Forms have finally been ported from Qt 3 to Qt 4.

- General:
    - **New:** Simplify and automatize bug reporting; OS and Platform information is auto-selected on bugs.kde.org.
    - **New:** Make side panes lighter by removing frames in all styles except Oxygen
    - **New:** Added "Close all tabs" action to main window tabs.
    - Improve appearance of main tabbed toolbar for GTK+ and Oxygen styles. (bug 341150)
    - Improve handling permission errors on database creation. Do not allow to create a new SQLite-based .kexi file if: non-writable folder is selected, relative path is selected (unsafe), non-file path is selected (perhaps a folder).
    - Do not crash when Kexi is unable to find plugins; display message and exit.
    - Fix right-to-left user interface support in side panes.
    - Simplify "Save password" checkbox text in database connection editor and add direct what's this button.
    - Disable ability of setting left/right sidebars floatable (like in Dolphin, improve stability)
    - Remove redundant 'find' action from the main toolbar. It's already available in local context where it really works.
    - Move the 'Export data table' from the main toolbar to a local table and query object's menu.
    - Improve user-visible messages.
- Forms:
    - **New:** Port Kexi Forms to Qt4's scroll area, a milestone leading to Qt5-based Kexi.
    - Improve translation support in Forms' action selection dialog
- Reports:
    - **New:** Added inline editing for labels in Report Designer.
    - **New:** Added "Do you want to open exported document?" question when report is exported to a Text/Spreadsheet/as Web Page.
    - Print reports in High DPI (precision). (bug 340598)

### Krita - Creative Sketching & Painting App

- **New:** Krita can now open multiple images in one window
- **New:** Perspective transform
- **New:** Liquify transform
- **New:** Cage transform
- **New:** Selection-shaped gradients
- **New:** Several new filters
- **New:** A HSV color selector
- **New:** It's now possible to edit the alpha channel separately
- **New:** A new feature to split a layer into several layers by color
- Thin line quality has been improved
- Anti-aliasing of the transform tool has been improved
- It's now much easier to create masks and convert between masks and layers
- Vector object scaling and resolution has been fixed
- The smudge brush has been made more correct
- Steps on the Undo history can now be merged
- The brush preset system has been improved to make it possible to temporarily lock changes to a preset during a session
- The G'Mic filter has been updated and there are previews now
- **Missing:** Photoshop layer styles and PSD layer masks: we're working hard on those, but they aren't done yet. We're working to have them ready by the end of January. The animation tool has been disabled for refactoring. In Beta 1, Sketch and Gemini have been disabled.

### Calligra Words - Word Processor

Layouting has been reworked to fix many small rendering glitches. It is the first required step before more page layouting features can be added as well as dynamic page layout changes.

## Try It Out

- The source code is available for download: [calligra-2.8.90.tar.xz](http://download.kde.org/unstable/calligra-2.8.90/calligra-2.8.90.tar.xz).
- Calligra binaries are available for [download](http://userbase.kde.org/Calligra/Download) for many operating systems.

## What's Next and How to Help?

We're approaching the era of [2.9](https://community.kde.org/Calligra/Schedules/2.9/Release_Plan) to be released in early 2015. It will be followed by Calligra 3.0 based on new technologies later in 2015.

You can meet us to share your thoughts or offer your support on [general Calligra forums](http://forum.kde.org/calligra) or dedicated [Kexi](https://forum.kde.org/kexi) or [Krita](https://forum.kde.org/krita) forums. Many improvements are only possible thanks to the fact that we're working together within the awesome community.

(some Calligra apps need new [maintainers](https://community.kde.org/Calligra/Maintainers), you can become one, it's fun!)

## How and Why to Support Calligra?

Calligra apps may be totally free, but their development is costly. Power, hardware, office space, internet access, travelling for meetings - everything costs. Direct donation is the easiest and fastest way to efficiently support your favourite applications. Everyone, regardless of any degree of involvement can do so. You can choose to:

- Support entire Calligra indirectly by donating to KDE, the parent organization and community of Calligra: [http://www.kde.org/community/donations](http://www.kde.org/community/donations),
- Support Krita directly by donating to the Krita Foundation, to support Krita development in general or development of a specific feature: [https://krita.org/support-us/donations](https://krita.org/support-us/donations/),
- Support Kexi directly by donating to its current BountySource fundraiser, supporting development of a specific feature, or the team in general: [https://www.bountysource.com/teams/kexi](https://www.bountysource.com/teams/kexi).

## About Calligra

Calligra Suite is a graphic art and office suite developed by the [KDE community](https://www.kde.org). It is available for desktop PCs, tablet computers, and smartphones. It contains applications for word processing, spreadsheets, presentation, databases, vector graphics, and digital painting. See more information at the website [http://www.calligra.org](http://www.calligra.org/).

<!-- h3 { padding-top:1em; } .button, .button:link, .button:visited { text-decoration:none; text-align:center; padding:11px 32px; border:solid 1px #004F72; -webkit-border-radius:10px; -moz-border-radius:10px; border-radius: 10px; font:18px Arial, Helvetica, sans-serif; font-weight:bold; color:#E5FFFF; background-color:#3BA4C7; background-image: -moz-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -webkit-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -o-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -ms-linear-gradient(top, #3BA4C7 0% ,#1982A5 100%); filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1982A5', endColorstr='#1982A5',GradientType=0 ); background-image: linear-gradient(top, #3BA4C7 0% ,#1982A5 100%); -webkit-box-shadow:0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; -moz-box-shadow: 0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; box-shadow:0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; } .button:hover{ padding:11px 32px; border:solid 1px #004F72; -webkit-border-radius:10px; -moz-border-radius:10px; border-radius: 10px; font:18px Arial, Helvetica, sans-serif; font-weight:bold; color:#E5FFFF; background-color:#3BA4C7; background-image: -moz-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -webkit-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -o-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -ms-linear-gradient(top, #3BA4C7 0% ,#1982A5 100%); filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1982A5', endColorstr='#1982A5',GradientType=0 ); background-image: linear-gradient(top, #3BA4C7 0% ,#1982A5 100%); -webkit-box-shadow:0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; -moz-box-shadow: 0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; box-shadow:0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; } .button:active{ padding:11px 32px; border:solid 1px #004F72; -webkit-border-radius:10px; -moz-border-radius:10px; border-radius: 10px; font:18px Arial, Helvetica, sans-serif; font-weight:bold; color:#E5FFFF; background-color:#3BA4C7; background-image: -moz-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -webkit-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -o-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -ms-linear-gradient(top, #3BA4C7 0% ,#1982A5 100%); filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1982A5', endColorstr='#1982A5',GradientType=0 ); background-image: linear-gradient(top, #3BA4C7 0% ,#1982A5 100%); -webkit-box-shadow:0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; -moz-box-shadow: 0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; box-shadow:0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; } <div></div> .button a,.button a:link, .button a:visited, .button a:hover, .button a:active { color:#E5FFFF; } -->
