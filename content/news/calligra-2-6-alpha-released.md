---
title: Calligra 2.6 Alpha Released
date: 2012-10-12
author: Inge Wallin
categories: []
---

The Calligra team is proud and pleased to announce the alpha release of version 2.6 of the [Calligra Suite](http://www.calligra-suite.org/ "Home page of the Calligra Suite"). This version is a preview of what will come in the final version of Calligra 2.6 that will be announced in december this year.

As usual, Calligra is developing very fast and there are a lot of new features to be announced. This is also the second feature update after the initial release of Calligra in April this year which means that we are approaching some form of maturity.The team will use a significant part of the remaining time until the release to polish away usability problems and visual glitches.

Calligra 2.6 beta will be announced in approximately a month. After this there will only be bugfixes on the release branch until the final release in December. Note that all new features are still work in progress until the beta release.

## News in This Release

As usual the release has a number of new or improved features. The most important of them will be highlighted here. There are still a few important improvements that we expect for the 2.6 beta and final versions but which were not ready in time for this alpha.

### New Application: Calligra Author

**Calligra Author** is a new member of the growing Calligra application family. The application was [announced just after the release of Calligra 2.5](http://www.calligra.org/news/calligra-announces-author/ ) with the following description:

> The application will support a writer in the process of creating an eBook from concept to publication. We have two user categories in particular in mind:
> 
> - Novelists who produce long texts with complicated plots involving many characters and scenes but with limited formatting.
> - Textbook authors who want to take advantage of the added possibilities in eBooks compared to paper-based textbooks.

The first version of Calligra Author will be very similar to Calligra Words since the new features of Author are also adopted by Words. Future releases may deviate more.

\[caption id="attachment\_3577" align="aligncenter" width="476"\][![Calligra Author](/assets/img/calligra-author-476x400.png "Calligra Author")](http://www.calligra.org/uncategorized/calligra-2-6-alpha-released/attachment/calligra-author/) Screenshot of Calligra Author using a very early version of the so-called "Distraction Free Mode". Content is from Irina Rempts novel Terms of Service which can be found at leebre.org.\[/caption\]

Features that were developed especially for Author include export to ebook formats EPUB2 and MOBI, and improved text statistics (word count, character count, etc).

### Productivity Applications

For the productivity part of the suite (word processor, spreadsheet, and presentation program) the target user of version 2.6 is still the student or academic user. This version has a number of new features that will make it more suitable for these users.

**Sheets**, the spreadsheet application, has a new function optimizer - also known as a Solver - and all standard scripts are now localizable.

**Stage**, the presentation program, has a new animation framework that lets the user create and manipulate animations in slideshows.

**Flow**, the diagram application, has improved connections.

**Kexi**, the visual database creator, has new support for user data storage, in improved CSV import and export and improved table views. Detailed list of changes for Kexi is [available](http://community.kde.org/Kexi/Releases/Kexi_2.6_Alpha).

### Artistic Applications

The artistic applications of the Calligra Suite are the most mature ones and are already used by professional users everywhere.

**Krita**, the drawing application, has new support for OpenColorIO, which makes it suitable for use in the movie industry. It also has big speedups in many places, including the [vc library](http://code.compeng.uni-frankfurt.de/projects/vc) and when painting using the new precision slider in the preset editor.

There is also improvements in painting HDR images and integration into a GFX workflow as well as support for the latest OpenRaster standard and many bugfixes.

\[caption id="attachment\_3589" align="aligncenter" width="500"\][![Screenshot of Krita 2.6 alpha](/assets/img/krita-lut-2.6-alpha-500x289.png "krita-lut-2.6-alpha")](http://www.calligra.org/news/calligra-2-6-alpha-released/attachment/krita-lut-2-6-alpha/) Setting Exposure on a HDR image with Krita 2.6's new OpenColorIO based LUT docker\[/caption\]

## Filters

There are two new filters in Calligra 2.6alpha:

- A new export filter for **EPUB2** files.
- A new export filter for **MOBI**.

And of course all the filters for **MS Office** have been improved even more, especially for the OOXML formats used in MS Office 2007 and 2010.

## Common Improvements

The architecture of the Calligra Suite lets the applications share much of the functionality of the suite with each other. Many common parts have seen improvements since the release of 2.5. Here are a few of them..

**Charts** have a number of improvements for fine tuning the formatting. Examples include fonts for titles and labels and markers for datasets.

**Mathematical formulas** have an improved rendering.

## Other Than the Desktop

**Calligra Active**, the version for tablets and smartphones, has several improvements: there is a new slide chooser for presentations, slides can be changed by flicking the device, page switching improvements for text documents and improved start up sequences including a new splash screen.

Calligra Active also takes advantage of the improvements in the common parts of Calligra: the libraries and plugins.

## Try It Out

The source code of the snapshot is available for download: [calligra-2.5.90.tar.bz2](http://master.kde.org/unstable/calligra-2.5.90/calligra-2.5.90.tar.bz2). As far as we are aware, the following distributions package Calligra 2.6alpha. This information will be updated when we get more details.

### Ubuntu

Users of Ubuntu and Kubuntu are urged to try the daily snapshots prepared by [Project Neon](https://launchpad.net/project-neon). Paste the following in a terminal window and you'll find Calligra installed in /opt:

sudo add-apt-repository ppa:neon/ppa \\
&& sudo apt-get update\\
&& sudo apt-get install project-neon-base \\
   project-neon-calligra \\
   project-neon-calligra-dbg

You can run these packages by adding /opt/project-neon/bin to your PATH.

### Arch Linux

Arch Linux provides Calligra packages in the \[kde-unstable\] repository.

### Fedora

Fedora packages are available in the rawhide development repo ([http://fedoraproject.org/wiki/Rawhide](http://fedoraproject.org/wiki/Rawhide)), and unofficial builds are available for prior releases from kde-unstable repo at [http://kde-redhat.sourceforge.net/](http://kde-redhat.sourceforge.net/) .

### OpenSuse

There are OpenSUSE Calligra packages in the [unstable playground repository](https://build.opensuse.org/project/show?project=KDE%3AUnstable%3APlayground).

### FreeBSD

Calligra ports are available in [Area51](http://freebsd.kde.org/area51.php).

### Windows and OSX

Windows packages will be available from [KO GmbH](http://www.kogmbh.com/). For download of Windows binaries, use the [download page](http://www.kogmbh.com/download.html). We would welcome volunteers who want to build and publish packages for the Calligra Suite on OSX.

## AboutCalligra

Calligra, comprising the Calligra Suite and Calligra Active, is part of the applications from the KDE community. See more information at the website [http://www.calligra.org/](http://www.calligra.org/).
