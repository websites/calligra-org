---
title: Calligra Plan version 3.3.0 released
date: 2021-01-28
author: danders
categories: [announcements,stable]
---

We are pleased to announce the relase of Calligra Plan 3.3.0.


** Main changes: **

```
* Allow to print a selectable time range.
* Fit printout to page height.
* Fit printout to a single page
* Print the chart on multiple pages.
* Uses a color palette suitable for printing on white paper
* Use the mouse or mouse weel to zoom the datetime scale.* Bug 415041 - Default calendar not set when creating new project
```
## Get it

Check your distribution's repositories to find and install Calligra Plan 3.3.0.

Tarball can be found here:

[https://download.kde.org/stable/calligra/3.3.0/calligraplan-3.3.0.tar.xz.mirrorlist](https://download.kde.org/stable/calligra/3.3.0/calligraplan-3.3.0.tar.xz.mirrorlist)
