---
title: Calligra 2.4.3 Released
date: 2012-06-27
author: cyrilleberger
categories: []
---

The Calligra team has released version 2.4.3, the third bugfix release of the [Calligra Suite](http://www.calligra.org/ "Home page of the Calligra Suite"), Calligra Mobile and Calligra Active. This release contains a number of important bug fixes to 2.4.2 and we recommend everybody to update as soon as possible.

## Bugfixes in This Release

Here is an overview of the most important fixes are in 2.4.3. There are several others that are not mentioned here:

### Words:

- Always show vertical scroll bar to avoid race condition (BUG: 301076)
- Do not save with an attribue that makes LibreOffice and OpenOffice crash (BUG: 298689 )

### Kexi:

- Fixed import from csv when "Start at Line" value changed (BUG: 302209)
- Set limit to 255 characters for Text type (VARCHAR) (BUG: 301277 and 301136)
- Remove limits for Text data type, leave as option (BUG:301277)
- Fixed data saving when focus policy for one of widgets is NoFocus (BUG: 301109)

### Krita:

- Read and set the resolution for psd images

### Charts:

- fix load/save styles of all shapes (title,subtitle,axistitles,footer,etc.)
- Lines in the chart should be displayed (BUG: 271771)
- Combined Bar and Line Charts only show bars (Trendlines not supported) (BUG:288537)
- load/save chart type for each dataset (BUG: 271771 and 288537)

## Try It Out

The source code of this version is available for download here: [calligra-2.4.3.tar.bz2](http://download.kde.org/stable/calligra-2.4.3/calligra-2.4.3.tar.bz2). Also many [binary packages](http://userbase.kde.org/Calligra/Download) are available for various operating systems and distributions. This release is packaged for Windows by [KO GmbH,](http://www.kogmbh.com/ "KO GmbH homepage") and you can find it on their [download page](http://www.kogmbh.com/download.html). There is also a separate package or [Krita](http://www.krita.org/) only.

We would welcome volunteers who want to build the Calligra Suite on OSX.

## About Calligra

Calligra is part of the applications from the KDE community. See more information at the website [http://www.calligra.org/](http://www.calligra.org/).
