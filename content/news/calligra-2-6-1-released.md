---
title: Calligra 2.6.1 Released
date: 2013-02-20
author: Inge Wallin
categories: []
---

The Calligra team has released version 2.6.1, the first bugfix release of the [Calligra Suite,  and Calligra Active](http://www.calligra.org/). This release contains a number of important bug fixes to 2.6.0 and we recommend everybody to update.

## Bugfixes in This Release

Here is an overview of the most important fixes that are in 2.6.1. There are several others that are not mentioned here. Since this release is very close to the big release of 2.6.0 the development team has concentrated on crash bugs and other serious bugs potentially leading to data loss.

### General:

- Fixes for several different **crashes**. See for instance bugs 314676, 314747
- Several **new or improved icons** for different applications.
- General **look and feel on Windows** improved by hardcoding the theme.

### Filters:

- A new filter for the **MOBI ebook format** was released with 2.6.1. This filter was scheduled for 2.6.0 but was withheld because of some bugs that were discovered late in the release cycle.
- Much improvement in exporting tables in the **HTML export** filter.
- Fix options in the **CSV export** dialog (bug 314766)

### Try It Out

- **The source code** is available for download: [calligra-2.6.1.tar.bz2]( http://download.kde.org/stable/calligra-2.6.1/calligra-2.6.1.tar.bz2). As far as we are aware, the following distributions package Calligra 2.6. This information will be updated when we get more details. In addition, many distributions will package and ship Calligra 2.6 as part of their standard set of applications.
- In **Chakra Linux**, Calligra is the default office suite so you don't have to do anything at all to try out Calligra.  Chakra aims to be a [showcase Linux for the "Elegance of the Plasma Desktop"](http://http://www.chakra-project.org/) and other KDE software.
- Users of **Ubuntu and Kubuntu** are urged to try the daily snapshots prepared by [Project Neon](https://launchpad.net/project-neon). Paste the following in a terminal window and you'll find Calligra installed in /opt:
    
    sudo add-apt-repository ppa:neon/ppa \\
    && sudo apt-get update\\
    && sudo apt-get install project-neon-base \\
       project-neon-calligra \\
       project-neon-calligra-dbg
    
    You can run these packages by adding /opt/project-neon/bin to your PATH.
- **Arch Linux** provides Calligra packages in the \[kde-unstable\] repository.
- **Fedora** packages are available in the rawhide development repository ([http://fedoraproject.org/wiki/Rawhide](http://fedoraproject.org/wiki/Rawhide)), and unofficial builds are available for prior releases from kde-unstable repository at [http://kde-redhat.sourceforge.net/](http://kde-redhat.sourceforge.net/) .
- There are **OpenSUSE** Calligra packages in the [unstable playground repository](https://build.opensuse.org/project/show?project=KDE%3AUnstable%3APlayground).
- Calligra **FreeBSD** ports are available in [Area51](http://freebsd.kde.org/area51.php).
- **MS Windows** packages will be available from [KO GmbH](http://www.kogmbh.com/). For download of Windows binaries, use the [download page](http://www.kogmbh.com/download.html).
- **Mac OS X:** We would welcome volunteers who want to build and publish packages for the Calligra Suite on OS X. There are some first attempts

## About Calligra

Calligra is part of the applications from the KDE community. See more information at the website [http://www.calligra.org/](http://www.calligra.org/).
