---
title: Calligra 2.9 Beta 2 Released
date: 2015-01-15
author: jstaniek
categories: []
---

We are happy to announce the **second beta release in 2.9 series** of [Calligra Suite](http://www.calligra-suite.org/ "Home page of the Calligra Suite") for you to test. We are focusing on fixing issues including those that you'd [report](http://bugs.kde.org/) so please continue to work with us. The next beta (3) is [expected](https://community.kde.org/Calligra/Schedules/2.9/Release_Plan#Stable_releases) in February 2015.

[Support Calligra!](https://www.calligra.org/news/calligra-2-9-beta-2-released/#support)

This time Kexi, Krita and Sheets improved most. After eliminating of all remaining dependencies on [Qt](http://en.wikipedia.org/wiki/Qt_%28software%29) 3, Calligra is now a fully Qt 4-based software, what forms a path to a Qt 5-based series.

## New Features and Improvements in This Release

### Kexi - Visual Database Applications Builder

[![New table view in Kexi 2.9](/assets/img/kexi-2.9-tableview_sm.png)](http://www.calligra.org/wp-content/uploads/2015/01/kexi-2.9-tableview.png) New table view in Kexi 2.9 Most importantly, tabular views (data grids) have finally been ported from Qt 3 to Qt 4, what took over 6 months of regular work.

- General:
    - **New:** Add dedicated [support](http://kexi-project.org/pics/2.9/kexi-2.9-breeze-icons.png "Dedicated support for Breeze icon theme of Plasma 5 in Kexi 2.9") for "Breeze" icon theme of Plasma 5.
    - Newly created project now does not use window of previously opened project. We're starting a new Kexi instance now. (bug 340768)
    - Make the project navigator [more modern](http://kexi-project.org/pics/2.9/kexi-2.9-project-navigator.png "Modern look for project navigator in Kexi 2.9"), a move towards Plasma 5. Tastes best with the Breeze style and icon theme.
- Table view:
    - **New:** Port tabular views (data grids) to Qt 4's scroll area, a milestone leading to Qt 5-based Kexi.
    - **New:** Display "key" icon in primary key columns.
    - Hide the "+" button in record navigator when inserting is off (e.g. in read-only mode or in queries).
- Queries:
    - Added "BETWEEN...AND" SQL operator support. (wish 331615)
- Reports:
    - **New:** Add "Interleaved 2 of 5" barcodes support (a port from OpenRPT). (wish 341473)
    - **New:** Add "Value" property for consistency to report items (check, field, text, barcode) providing contain data that is displayed when the item isn't bound to a database field.
    - Fix possible hang in text report item.

### Krita - Digital Painting for Artists

[![Multiple document support in Krita 2.9](/assets/img/krita-2.9_multi-document_sm.png)](http://www.calligra.org/wp-content/uploads/2015/01/krita-2.9_multi-document.jpg) Multiple document support in Krita 2.9

- Fixes and improvements with how actions and shortcuts are managed. This will help fix a lot of shortcut conflicts that existed in the past. Actions and functionality are smarter now as well. This means that buttons and menu items will be disabled/enabled when they are not available. (eg. You cannot change the canvas size if there is no documents open).
- add drag and drop when there are no documents open
- make the background configurable with a color or image (located in Settings > Configure Krita)
- fixed a lot of multiple document bugs
- rename "hairy" brush engine to "bristle" brush engine
- add "flatten" action to layer context menu
- PDF export - landscape and portrait now work correctly
- initial tool when loaded is freehand brush
- don't show menu icons unless you are using KDE
- added a lot of presets for color profiles from Elle Stone
- fix handling of locked dockers behaves better when changing workspaces
- make it possible to convert images using 'krita --export bla.png --export-filename bla.jpg'
- fix crash when mergine a multi-selection of layers that includes a group layer
- Removed the selection brush; use the global selection mask and the regular brush tools instead
- Make krita open new images in the current window
- On Linux: use colord to get the system monitor profile, and make the system monitor profiles work in a multi-monitor setup
- Fix the layout of the filter and filter layer dialogs.
- Make it possible to use different OpenColorIO settings for each canvas
- Improve startup speed on all platforms
- Mirror axis tools now follow the rotation of the canvas
- fix rotation in brush preview
- fixed loading assistants
- fixed toggling of painting assistants
- improve how the color slider widget is used
- resource bundle fixes with paint operations, loading tags, and double loading resources
- prevent loading corrupt resource bundles (added MD5 Checksums)
- fix lockup with perspective assistants
- improve the color smudge brush engine so it doesn't darken too much
- lots of fixes for working with resource bundles
- bug fixes when selecting shapes
- fixed some computer systems that weren't loading preset bundles correctly
- fixed color selector crashes when no document open
- fix switching between landscape and portrait orientation
- lots of fixes with handling multiple open images and the dockers
- update gmic to version 1.6.0.3
- enable interactive filters like [colorize \[interactive\]](http://www.davidrevoy.com/article240/gmic-line-art-colorization)
- implement progress reporting when applying filter
- if gmic script fails, inform user about the error message
- fixed lost filtering when you select filter and click ok
- fixed a bunch of multi-document specific issues
- fixes when selecting colors with the advanced color selector
- brush sizes are back to having 2 decimals to prevent issues
- fixed a few bugs related to the crop tool with properties and resetting
- fixed saving split alpha mask
- fixed a lot of bugs in the dirty presets feature
- fixed mirror tool working with shapes like circles and rectangles
- fix shaky lines on 32-bit systems when using canvas rotation
- fixes to the cage transform tool
- Improve handles for the transform tool
- fixes to the opacity slider
- fixes to the tablet handling (make working with vector shapes with the tablet possible again)
- make the liquify tool handle build-up/wash mode properly
- new color smudge presets
- Fix a bunch memory leaks and double deletions
- Fix crash when adding drop shadow effect
- Fix crash when closing a document while moving the stylus
- Fix the opengl canvas for some (notably AMD drivers). More fixes coming up!
- updated layout for resource manager
- mirror tools have updated UI
- G'MIC has been moved to filters in the menu
- Resource manager has moved to settings
- "Size Canvas to Size of Current Layer" is renamed to "Trim to Current Layer" and "Size Canvas to Size of Selection" is renamed to "Trim to Selection"
- Lots of warnings fixed that only show up with clang, quite a few of which unconvered real bugs.
- Fix thumbnailing for .kra and .ora files
- Add a shortcut to toggle the assistants on and off
- Fix pasting text in a multi-line text shape

### Calligra Plan - Project Management

- Make icons match the proper size.

### Calligra Sheets - The Spreadsheet

- Speed up drawing of complex documents
- Updated default currency to Euro for Lithuania
- Do not crash in the table shape when the external editor is not available
- Fix up mouse interaction with the table shape
- Fix a problem with non-native files not being openable
- Fix sorting to properly update formulas instead of discarding them
- Fix Find/Replace to not skip the last row
- Fix a deadlock within a COUPNUM function

## Try It Out

- The source code is available for download: [calligra-2.8.91.tar.xz](http://download.kde.org/unstable/calligra-2.8.91/calligra-2.8.91.tar.xz).
- Calligra binaries are available for [download](http://userbase.kde.org/Calligra/Download) for many operating systems.

## What's Next and How to Help?

We're approaching the era of [2.9](https://community.kde.org/Calligra/Schedules/2.9/Release_Plan) to be released in early 2015. It will be followed by Calligra 3.0 based on new technologies later in 2015.

You can meet us to share your thoughts or offer your support on [general Calligra forums](http://forum.kde.org/calligra) or dedicated [Kexi](https://forum.kde.org/kexi) or [Krita](https://forum.kde.org/krita) forums. Many improvements are only possible thanks to the fact that we're working together within the awesome community.

(Some Calligra apps need new [maintainers](https://community.kde.org/Calligra/Maintainers), you can become one, it's fun!)

## How and Why to Support Calligra?

Calligra apps may be totally free, but their development is costly. Power, hardware, office space, internet access, travelling for meetings - everything costs. Direct donation is the easiest and fastest way to efficiently support your favourite applications. Everyone, regardless of any degree of involvement can do so. You can choose to:

- Support entire Calligra indirectly by donating to KDE, the parent organization and community of Calligra: [http://www.kde.org/community/donations](http://www.kde.org/community/donations),
- Support Krita directly by donating to the Krita Foundation, to support Krita development in general or development of a specific feature: [https://krita.org/support-us/donations](https://krita.org/support-us/donations/),
- Support Kexi directly by donating to its current BountySource fundraiser, supporting development of a specific feature, or the team in general: [https://www.bountysource.com/teams/kexi](https://www.bountysource.com/teams/kexi).

## About Calligra

Calligra Suite is a graphic art and office suite developed by the [KDE community](https://www.kde.org). It is available for desktop PCs, tablet computers, and smartphones. It contains applications for word processing, spreadsheets, presentation, databases, vector graphics, and digital painting. See more information at the website [http://www.calligra.org](http://www.calligra.org/).

<!-- h3 { padding-top:1em; } .button, .button:link, .button:visited { text-decoration:none; text-align:center; padding:11px 32px; border:solid 1px #004F72; -webkit-border-radius:10px; -moz-border-radius:10px; border-radius: 10px; font:18px Arial, Helvetica, sans-serif; font-weight:bold; color:#E5FFFF; background-color:#3BA4C7; background-image: -moz-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -webkit-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -o-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -ms-linear-gradient(top, #3BA4C7 0% ,#1982A5 100%); filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1982A5', endColorstr='#1982A5',GradientType=0 ); background-image: linear-gradient(top, #3BA4C7 0% ,#1982A5 100%); -webkit-box-shadow:0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; -moz-box-shadow: 0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; box-shadow:0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; } .button:hover{ padding:11px 32px; border:solid 1px #004F72; -webkit-border-radius:10px; -moz-border-radius:10px; border-radius: 10px; font:18px Arial, Helvetica, sans-serif; font-weight:bold; color:#E5FFFF; background-color:#3BA4C7; background-image: -moz-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -webkit-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -o-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -ms-linear-gradient(top, #3BA4C7 0% ,#1982A5 100%); filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1982A5', endColorstr='#1982A5',GradientType=0 ); background-image: linear-gradient(top, #3BA4C7 0% ,#1982A5 100%); -webkit-box-shadow:0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; -moz-box-shadow: 0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; box-shadow:0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; } .button:active{ padding:11px 32px; border:solid 1px #004F72; -webkit-border-radius:10px; -moz-border-radius:10px; border-radius: 10px; font:18px Arial, Helvetica, sans-serif; font-weight:bold; color:#E5FFFF; background-color:#3BA4C7; background-image: -moz-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -webkit-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -o-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -ms-linear-gradient(top, #3BA4C7 0% ,#1982A5 100%); filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1982A5', endColorstr='#1982A5',GradientType=0 ); background-image: linear-gradient(top, #3BA4C7 0% ,#1982A5 100%); -webkit-box-shadow:0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; -moz-box-shadow: 0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; box-shadow:0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; } <div></div> .button a,.button a:link, .button a:visited, .button a:hover, .button a:active { color:#E5FFFF; } -->
