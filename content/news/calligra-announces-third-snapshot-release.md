---
title: Calligra Announces Third Snapshot Release
date: 2011-07-13
author: boudewijn
categories: [announcements]
---

The Calligra Application Suite team is releasing the third step towards its first full release: the third snapshot release. Following hot on the heels of last month's snapshot, this release contains improvements everywhere, and after all the work on the engine, now the applications themselves are getting tender loving care as well.

The text layout engine once again received a lot of attentions, with many small layout issues being fixed, as well as some major performance issues. The table-of-contents feature now can be accessed from the gui. For Tables, the rendering of the row and column headers has been improved, while Krita has again seen many commits, adding for instance configurable sliders for opacity, size and flow. Stage has a brand-new slide sorter that's a joy to work with.

But not all the coolness the Calligra team is hacking is appearing already in this snapshot release. With over a hundred feature branches, the developers are taking to the git-based workflow where a feature is perfected in a branch and then merged like wasps take to jam. Many of these features will also land in time for the first Calligra release later this year.

### Get It

The source code of the snapshot is available for download: [calligra-2.3.73.tar.bz2](http://download.kde.org/download.php?url=unstable/calligra-2.3.73/calligra-2.3.73.tar.bz2)

#### Ubuntu

Users of Ubuntu and Kubuntu are urged to try the daily snapshots prepared by [Project Neon](https://launchpad.net/project-neon). Paste the following in a terminal window and you'll find Calligra installed in /opt:

sudo add-apt-repository ppa:neon/ppa \\
&& sudo apt-get update\\
&& sudo apt-get install project-neon-base \\
   project-neon-calligra \\
   project-neon-calligra-dbg

You can run these packages by adding /opt/project-neon/bin to your PATH.

#### Arch Linux

Arch Linux provides Calligra packages in the \[kde-unstable\] repository.

#### Fedora

Fedora is working on packages and expects them to be available within the next two weeks.

#### OpenSuse

There are OpenSUSE Calligra packages in the [unstable playground repository](https://build.opensuse.org/project/show?project=KDE%3AUnstable%3APlayground).

#### FreeBSD

Calligra ports are available in [Area51](http://freebsd.kde.org/area51.php).

#### Windows and OSX

Work has started on a Windows package of the Calligra Application Suite but we are not done yet, so for a Windows build, you will have to wait. We would welcome volunteers who want to build the Calligra Suite on OSX.
