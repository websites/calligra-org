---
title: Calligra 2.5 Beta Released
date: 2012-06-23
author: Inge Wallin
categories: []
---

The Calligra team is proud and pleased to announce the beta release of version 2.5 of the [Calligra Suite](http://www.calligra-suite.org/ "Home page of the Calligra Suite"). This means that the calligra/2.5 branch has been created and there are no further features allowed until the real release approximately a month from now.

## News in This Release

As expected, this release has a number of new features that were not ready in time for the alpha release. Here are some of the most important ones:

**Sheets**, the spreadsheet application has a new stand-alone docker for the cell editor. In Version 2.4 of Calligra Sheets, the cell editor was a standard tool docker, which meant that most people kept it in the side bar. Some users thought this was a disadvantage because the editor area was too narrow. In version 2.5 the cell editor is now contained in its own dock window which means that it can be placed above the work area as in other spreadsheets or even be torn off into its own window.

**Sheets** also has a new cell tool window with cell formatting controls. In version 2.4 Sheets used mostly menus and dialogs for formatting cells. In version 2.5 the most common formatting options are collected into a cell tool option window much like the text formatting controls in Words.

\[caption id="attachment\_3415" align="aligncenter" width="300"\][![](/assets/img/sheets_new_ui-300x200.png "sheets_new_ui")](http://www.calligra.org/news/calligra-2-5-beta-released/attachment/sheets_new_ui/) New Ui in Calligra Sheets 2.5\[/caption\]

An improvement **common to all the applications** is a new system for connecting shapes. This is used for instance when creating diagrams for flowcharts or organizational charts. The new connectors in Calligra 2.5 are both more powerful and easier to use than those in version 2.4.

**Krita**, the painting application, has an improved canvas handling subsystem. This means that rotation and scaling of the canvas can now be done when any other tool is active. This should improve the interaction for artists that make much use of the canvas manipulation feature.

**Kexi**, the database application, now offers a full screen mode activated by the F11 key. New form elements Command Link Button, Slider, Progress Bar and Date Picker widgets have been added to Forms.

\[caption id="attachment\_3426" align="aligncenter" width="300"\][![](/assets/img/kexi-2.5-new-form-widgets-300x200.png "New form widgets in Kexi 2.5")](http://www.calligra.org/news/calligra-2-5-beta-released/attachment/kexi-2-5-new-form-widgets/) New form widgets in Kexi 2.5\[/caption\]

## Try It Out

The source code of the snapshot is available for download: [calligra-2.4.91.tar.bz2](http://download.kde.org/unstable/calligra-2.4.91/calligra-2.4.91.tar.bz2). Alternatively, you can [download binaries for many Linux distributions and windows](http://userbase.kde.org/Calligra/Download#Unstable_Release).

## About the Calligra Suite

The Calligra Suite is part of the applications from the KDE community. See more information at the website [http://www.calligra.org/](http://www.calligra.org/).
