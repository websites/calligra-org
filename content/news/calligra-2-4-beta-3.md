---
title: Calligra 2.4 Beta 3
date: 2011-10-26
author: Inge Wallin
categories: [announcements]
---

The Calligra team is proud and excited to release the third beta version of the [Calligra Suite](http://www.calligra-suite.org/ "Home page of the Calligra Suite"). Since the start of the Calligra effort, we have had a long and very productive development phase. In the mean time we have released four snapshot versions, also known as alphas, to give the users a chance to see what we are doing.

This third beta is the result of the continuous polishing work done by the development team. We are aiming for the final release in November 2011, but already now you can see many bugfixes and improvements. Look in the [full list of changes](http://www.calligra-suite.org/changelogs/calligra-2-4-beta-3-changelog "Changelog for Calligra 2.4 beta3") for more details.

News in This Release

The highlights of this release include:

Productivity Applications

For the productivity part of the suite (word processor, spreadsheet, and presentation program) the target user of version 2.4 is the student or academic user. This version has a number of new features that will make it more suitable for these users.

**Words**, the word processor, has improvements for lists, page numbers and footnotes.

**Tables**, the spreadsheet application, has improvements for rotated text in cells.

**Kexi**, the database application, has many small fixes including a crash fix in handling of CSV files.

**Plan**, the project management application**,** has improvements in scheduling and also remembers some settings better now.

### Artistic Applications

The artistic applications of the Calligra Suite are the most mature ones and are already used by professional users everywhere.

**Krita**, the painting application, has a many bugfixes. One noteworthy thing is the new import filter for RGB files from Photoshop.

**Karbon**, the vector drawing application, has some fixes in updating the on-screen image.

Common Improvements

The architecture of the Calligra Suite lets the applications share much of the functionality of the suite with each other. Many common parts have seen improvements since the last beta release. Here are a few of them. For full details, see again the list of changes.

- Improvements in text layout.
- Some big dependencies have been removed.
- The stand-alone file converter _calligraconverter_ can now also convert to PDF. Previously this was done by the printer code.
- Split view has been removed from the UI since this never worked well. A new solution for this may return in a later version of Calligra but not for 2.4.
- Support for rectangular and square gradients.
- Improvements in conversions from Microsoft formats, especially DOCX and PPTX.

The native file format of Calligra is the Open Document Format, ODF. Compatibility with other suites and formats has always been a priority and as usual all the import filters for Microsoft formats have received attention and improvements. At this time, the Calligra import filters for docx/xlsx/pptx are the best free translations tools available anywhere.

## Try It Out

The source code of this version is available for download here: [calligra-2.3.83.tar.bz2](http://download.kde.org/download.php?url=unstable/calligra-2.3.83/calligra-2.3.83.tar.bz2)

### Ubuntu

Users of Ubuntu and Kubuntu are urged to try the daily snapshots prepared by [Project Neon](https://launchpad.net/project-neon). Paste the following in a terminal window and you'll find Calligra installed in /opt:

sudo add-apt-repository ppa:neon/ppa \\
&& sudo apt-get update\\
&& sudo apt-get install project-neon-base \\
   project-neon-calligra \\
   project-neon-calligra-dbg

You can run these packages by adding /opt/project-neon/bin to your PATH.

### Arch Linux

Arch Linux provides Calligra packages in the \[kde-unstable\] repository.

### Fedora

Fedora is working on packages and expects them to be available within the next two weeks.

### OpenSuse

There are OpenSUSE Calligra packages in the [unstable playground repository](https://build.opensuse.org/project/show?project=KDE%3AUnstable%3APlayground).

### Gentoo

Gentoo has Calligra packaged in Portage, tagged as unstable.

### FreeBSD

Calligra ports are available in [Area51](http://freebsd.kde.org/area51.php). Beta 3 may not be ready at the time of this announcement but will be ready soon after according to the packagers.

### Windows and OSX

[KO GmbH](http://www.kogmbh.com/ "KO GmbH homepage") will release a windows build of this beta version very soon. This post will be updated when that happens.

We would welcome volunteers who want to build the Calligra Suite on OSX.

## About the Calligra Suite

The Calligra Suite is part of the applications from the KDE community. See more information at the website [http://www.calligra-suite-org/](http://www.calligra-suite.org/).
