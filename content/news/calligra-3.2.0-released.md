---
title: Calligra 3.2
layout: post
date: 2020-04-29
---

We are happy to announce the immediate release of Calligra 3.2. This release contains two years of work carried out on KDE's office suite and brings improvements to Gemini, new features to Karbon, and many bug fixes and improvements that make Calligra more compatible with LibreOffice.

## [Gemini](/gemini)

Calligra Gemini is an office suite especially designed for 2 in 1 devices, that is, for laptops with touchscreens that can double up as tablets.

In this version, Calligra Gemini's welcome pages have been ported to Kirigami. Kirigami is an advanced KDE framework that allows developers create apps that work both on desktop and mobile devices. This means that, as a user, you will find Gemini's controls and layout familiar and easier to understand. It also guarantees more stability and a quicker development rate for Gemini, ensuring the suite will be kept up to date and will be more reliable for you. New developers wanting to contribute to Gemini will also find it easier to join the project.

The touch view accepts more touch events and we have improved the switch to change between the touch and desktop view.

On the cloud front, some we have fixed some crashes by postponing some of the loading processes and the Dropbox support has been migrated away from the deprecated interfaces.

![Calligra Gemini welcome screen](/assets/img/gemini-welcome.png)

## [Karbon](/karbon)

Karbon now supports document containing multiple pages. You can also import PDFs with multiples pages and, when exporting a document containing multiple pages to an image, you can choose which page to export.

We also improved Karbon's toolbar which you can now scroll when using a lot of tools or with a small screen.

![Karbon](/assets/img/karbon-complex.png)

## [Stage](/stage)

Stage, the Calligra presentation editor, now supports automatic slide transition, with two modes:

* Manual: Slide transition and animations must be activated manually (any page effect is always run)
* Automatic: Slide transition included page effects and animations are activated automatically after a user-selected time.

You can now toggle page margins and we have fixed sequential and parallel animations.

![Stage](/assets/img/stage.jpg)

## More

Read the [full changelog](/log/CHANGELOG-3.2.0.txt) to see all the improvements and bug fixes that went into this release.

## Get it

Check your distribution's repositories to find and install the Calligra 3.2.
