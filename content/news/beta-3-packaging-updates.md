---
title: Beta 3 Packaging Updates
date: 2011-11-02
author: boudewijn
categories: []
---

Since releasing Calligra 2.4, beta 3 we've received three updates from packagers!

First came [Andreas Huettel for Gentoo](http://dilfridge.blogspot.com/2011/10/towards-calligra-24.html). He says:

> I've just committed the ebuild for [Calligra 2.4 beta 3](http://www.calligra-suite.org/news/announcements/calligra-2-4-beta-3/), i.e. in Gentoo [app-office/calligra-2.3.83](http://packages.gentoo.org/package/app-office/calligra), to the portage tree. If all goes well this is going to be the last beta version before Calligra 2.4

Gentoo users rejoice!

Then Jonathan Riddel told me that Paolo Dias has packaged the latest beta for \*buntu -- \*buntu users now have a choice between project Neon (follows git master) and the official releases of the Calligra and Krita teams. This is the ppa:

https://edge.launchpad.net/~paulo-miguel-dias/+archive/peppa/+packages

It's experimental of course, but very, very welcome!

Finally, for Debian users there's great news: the [qt-kde debian project](http://qt-kde.debian.net/) has packages available as well now, thanks to Adrien. Right now it's beta2, but when Adrien returns from his well-earned vacation, he will update it to beta 3. Just follow these instructions:

According to the webpage (http://qt-kde.debian.net/), You have to add the following to your /etc/apt/sources.list:

deb http://qt-kde.debian.net/debian experimental-snapshots main
deb-src http://qt-kde.debian.net/debian experimental-snapshots main

In order to get repository key, install the pkg-kde-archive-keyring package:

aptitude install pkg-kde-archive-keyring

then you can install calligra with :

apt-get install calligra

And if you are interested in [Krita](http://www.krita.org), join us next Saturday on #krita on freenode for the bug day! Help us find bugs so we can fix them in time for the next release!
