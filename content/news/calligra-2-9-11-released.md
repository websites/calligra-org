---
title: Calligra 2.9.11 Released
date: 2016-02-03
author: jstaniek
categories: []
---

We are pleased to announce availability of the [Calligra Suite, and Calligra Active 2.9.11](http://www.calligra.org). It is recommended update for the 2.9 series of the applications and underlying development frameworks.

[Support Calligra!](https://www.calligra.org/news/calligra-2-9-11-released/#support)

## Bug fixes in This Release

You will find most of the improvements in Krita and Kexi. There are several others that may be not mentioned here.

### General

- A number of fixes for charts (review 122153).

### Kexi

**Featured story:** A commercially developed upgrade for MS Access (MDB) Import to Kexi has been donated by Jarosław Staniek (wish 277583). This also fixes import of primary keys (bug 336560). Large Access databases such as the Northwind sample now import better than ever.

Other Improvements:

- _Main Window_:
    - Make Global Search list should wider to fit the names (bug 358203).
    - Make the default "Smallest readable font" used by Kexi in property editor, toolbar, etc.; allow to specify size using points; no longer depend on screen resolution (bug 358662).

- _Tables_:
    - Do not switch to Data View if fetching records failed, and show more clear message. This fixes crash at the attempt to create a table row when table is invalid due to use of a reserved word or failed data fetching (bugs 356888 and 357025). The same category of errors are now also fixed for queries, forms, reports, and CSV export.
    - Fix alignment of table view combo boxes when rows or columns are scrolled (bug 357655).
    - Make combo box popups for tabular view fit in the current screen (bug 357682).

- _Queries_:
    
    - Switching off the visibility of query fields no longer hides data in the last field. This is a fix for SQLite, MySQL, PostreSQL, Sybase/MSSQL (bug 346839).
    - Fix possible crash due to missing removal of cached field information.
    - Support queries having the same field name used in multiple tables (bug 340056).
    - Make "Save button" enabled when going from SQL view to Data and back to SQL (bug 278379).
    - Fix: Changes in query design not retained after changes in SQL view and switching to Data view (bug 358413).
    - Make <SELECT "a" + "b"> SQL queries work; also for convenience if either argument is of text type, operator "+" is now assumed to be identical to operator "||"; also fix <SELECT "a" || "b"> for MySQL using CONCAT() (bug 358636).
    - Better check result of parsing SQL statements.
    - Make query parameters also work in COLUMNS section, e.g. <SELECT \[Param\]> works now (bug 348473).
    - Fix switching to Data View when using table aliases (bug 278414).
    
      
    

- _Forms_:
    
    - Add sorting to Forms. Current record is unchanged but its index most likely changes, what can be observed in the record navigator (bug 150372).
    
      
    

- _Reports_:
    
    - Fix retrieval of aggregate report values for queries: fix possible crash when changing data source, support tables and queries having the same names, better recognize if report was modified (reviews D764 and D765).
    - Fix handling latitude property for scripting in Report Map element.
    - Fix possible memory leaks (review D780).
    - Allow jump to report page (bug 357217).
    
      
    

### Krita

- Fix a memory leak when images are copied to the cliboard
- Ask the user to wait or break off a long-running operation before saving a document
- Update to G'Mic 1.6.9-pre
- Fix rendering of layer styles
- Fix a possible issue when loading files with clone layers
- Do not crash if there are monitors with negative numbers
- Make sure the crop tool always uses the correct image size
- Fix a crash on closing images while a long-running operation is still working
- Link to the right JPEG library
- Fix the application icon
- Fix switching colors with X after using V to temporarily enable the line tool
- Fix the unreadable close button on the splash screen when using a light theme
- Fix the Pencil 2B preset
- Fix the 16f grayscale colorspace to use the right channel positions
- Add shortcuts to lower/raise the current layer

### Calligra Sheets

- Fix calculation of WEEKSINYEAR() function, December 31 should not belong to first week of next year.

### Extras

- Fix Table of Contents generation for Okular ODT generator.

## Try It Out

![Download small](/assets/img/download-70.png)

The source code of the release is available for download here: [calligra-2.9.11.tar.xz](http://download.kde.org/stable/calligra-2.9.11/calligra-2.9.11.tar.xz). Also [translations to many languages](http://download.kde.org/stable/calligra-2.9.11/calligra-l10n/) and [MD5 sums](http://download.kde.org/stable/calligra-2.9.11/MD5SUMS). Alternatively, you can [download binaries for many Linux distributions and for Windows](http://userbase.kde.org/Calligra/Download#Stable_Release) (users: feel free to update that page).

  

## What's Next and How to Help?

The next step after the [2.9 series](https://community.kde.org/Calligra/Schedules/2.9/Release_Plan) is Calligra 3.0 which will be based on new technologies. We expect it later in 2016.

You can meet us to share your thoughts or offer your support on [general Calligra forums](http://forum.kde.org/calligra) or dedicated to [Kexi](https://forum.kde.org/kexi) or [Krita](https://forum.kde.org/krita). Many improvements are only possible thanks to the fact that we're working together within the awesome community.

(Some Calligra apps need new [maintainers](https://community.kde.org/Calligra/Maintainers), you can become one, it's fun!)

## How and Why to Support Calligra?

Calligra apps may be totally free, but their development is costly. Power, hardware, office space, internet access, travelling for meetings - everything costs. Direct donation is the easiest and fastest way to efficiently support your favourite applications. Everyone, regardless of any degree of involvement can do so. You can choose to: ![Heart](/assets/img/heart-701.png)

**Support entire Calligra indirectly by donating to KDE**, the parent organization and community of Calligra: [http://www.kde.org/community/donations](http://www.kde.org/community/donations).

![Heart](/assets/img/heart-701.png)

**Support Krita directly by donating to the Krita Foundation**, to support Krita development in general or development of a specific feature: [https://krita.org/support-us/donations](https://krita.org/support-us/donations/).

![Heart](/assets/img/heart-701.png)

**Support Kexi directly by donating to its current BountySource fundraiser**, supporting development of a specific feature, or the team in general: [https://www.bountysource.com/teams/kexi](https://www.bountysource.com/teams/kexi).

## About the Calligra Suite

![](/assets/img/calligra-logo-transparent-for-light-600-300x200.png)

Calligra Suite is a graphic art and office suite developed by the KDE community. It is available for desktop PCs, tablet computers and smartphones. It contains applications for word processing, spreadsheets, presentation, databases, vector graphics and digital painting. For more information visit [calligra.org](http://www.calligra.org/).

  

## About KDE

![](/assets/img/klogo-official-lineart_simple_bw-128x128.png)

[KDE is an international technology team](https://www.kde.org/community/whatiskde/) that creates free and open source software for desktop and portable computing. Among KDE's products are a modern desktop system for Linux and UNIX platforms, comprehensive office productivity and groupware suites and hundreds of software titles in many categories including Internet, multimedia, entertainment, education, graphics and software development. KDE's software available in more than 60 languages on Linux, BSD, Solaris, Windows and Mac OS X.

<!-- h3 { padding-top:1em; } .button, .button:link, .button:visited { text-decoration:none; text-align:center; padding:11px 32px; border:solid 1px #004F72; -webkit-border-radius:10px; -moz-border-radius:10px; border-radius: 10px; font:18px Arial, Helvetica, sans-serif; font-weight:bold; color:#E5FFFF; background-color:#3BA4C7; background-image: -moz-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -webkit-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -o-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -ms-linear-gradient(top, #3BA4C7 0% ,#1982A5 100%); filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1982A5', endColorstr='#1982A5',GradientType=0 ); background-image: linear-gradient(top, #3BA4C7 0% ,#1982A5 100%); -webkit-box-shadow:0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; -moz-box-shadow: 0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; box-shadow:0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; } .button:hover{ padding:11px 32px; border:solid 1px #004F72; -webkit-border-radius:10px; -moz-border-radius:10px; border-radius: 10px; font:18px Arial, Helvetica, sans-serif; font-weight:bold; color:#E5FFFF; background-color:#3BA4C7; background-image: -moz-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -webkit-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -o-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -ms-linear-gradient(top, #3BA4C7 0% ,#1982A5 100%); filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1982A5', endColorstr='#1982A5',GradientType=0 ); background-image: linear-gradient(top, #3BA4C7 0% ,#1982A5 100%); -webkit-box-shadow:0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; -moz-box-shadow: 0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; box-shadow:0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; } .button:active{ padding:11px 32px; border:solid 1px #004F72; -webkit-border-radius:10px; -moz-border-radius:10px; border-radius: 10px; font:18px Arial, Helvetica, sans-serif; font-weight:bold; color:#E5FFFF; background-color:#3BA4C7; background-image: -moz-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -webkit-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -o-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -ms-linear-gradient(top, #3BA4C7 0% ,#1982A5 100%); filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1982A5', endColorstr='#1982A5',GradientType=0 ); background-image: linear-gradient(top, #3BA4C7 0% ,#1982A5 100%); -webkit-box-shadow:0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; -moz-box-shadow: 0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; box-shadow:0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; } <div></div> .button a,.button a:link, .button a:visited, .button a:hover, .button a:active { color:#E5FFFF; } -->
