---
title: Mini sprint about anchoring and text run around
date: 2011-09-03
author: boemann
categories: []
---

Following the previous successful mini sprints yet another is about to take place in Copenhagen, Denmark Sunday 4th to Wednesday 7th.

A mini sprint consists of only 2-3 people intensely focusing on a very specific problem. Previously the textdocument tables were implement with sponsoring of KO GmbH, and presentation animation with sponsoring of KDE e.V. This time the topic is the anchoring of objects to a specific point in text and the text run around objects.

[![](/assets/img/spring-500x300.png "Words_spring")](http://www.calligra-suite.org/?attachment_id=1938)

The new textlayout engine already handles it but the topic is so complex that we keep running into small bugs. So the purpose of the mini sprint is to finally settle all issues once and for all, and to make sure we have unit tests that will prevent future regressions.

The KDE e.V. have kindly offered to sponsor this mini sprint
