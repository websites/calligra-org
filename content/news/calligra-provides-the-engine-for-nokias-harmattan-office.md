---
title: Calligra provides the Engine for Nokias Harmattan Office
date: 2011-10-24
author: cyrilleberger
categories: []
---

Mobile giant Nokia has just released the Linux based N9 model. Among the applications that are bundled with the smartphone the user will find the office application Harmattan Office.

Harmattan Office is a document viewer based on the Calligra Office Engine which is part of the open-source Calligra Suite. This application provides an advanced viewer for documents in the OpenDocument Format, binary Microsoft Office documents, and Microsoft OOXML documents, as well as PDF.

Nokia has developed the Harmattan Office application together with the Calligra community and companies like KO GmbH. The result is the best mobile office document viewer available anywhere, outperforming all comparable proprietary applications on smartphone platforms.

Thorsten Zachmann from Nokia said in his presentation at the Free Desktop Summit in Berlin this summer that soon after the N9 model is released, the Harmattan Office UI will be released as open source software. The improvements to the Calligra Office Engine are already in the Calligra source repository. This is the effect of Nokia working in cooperation with the community.
