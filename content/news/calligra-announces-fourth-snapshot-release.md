---
title: Calligra Announces Fourth Snapshot Release
date: 2011-09-02
author: cyrilleberger
categories: []
---

The Calligra Application Suite team is releasing the fourth step towards its first full release: the fourth snapshot release. Following hot on the heels of last month's snapshot, this release contains improvements everywhere, and after all the work on the engine, now the applications themselves are getting tender loving care as well.

### Get It

The source code of the snapshot is available for download: [calligra-2.3.74.tar.bz2](http://download.kde.org/download.php?url=unstable/calligra-2.3.74/calligra-2.3.74.tar.bz2)

#### Ubuntu

Users of Ubuntu and Kubuntu are urged to try the daily snapshots prepared by [Project Neon](https://launchpad.net/project-neon). Paste the following in a terminal window and you'll find Calligra installed in /opt:

sudo add-apt-repository ppa:neon/ppa \\
&& sudo apt-get update\\
&& sudo apt-get install project-neon-base \\
   project-neon-calligra \\
   project-neon-calligra-dbg

You can run these packages by adding /opt/project-neon/bin to your PATH.

#### Arch Linux

Arch Linux provides Calligra packages in the \[kde-unstable\] repository.

#### Fedora

Fedora is working on packages and expects them to be available within the next two weeks.

#### OpenSuse

There are OpenSUSE Calligra packages in the [unstable playground repository](https://build.opensuse.org/project/show?project=KDE%3AUnstable%3APlayground).

#### FreeBSD

Calligra ports are available in [Area51](http://freebsd.kde.org/area51.php).

#### Windows and OSX

Work has started on a Windows package of the Calligra Application Suite but we are not done yet, so for a Windows build, you will have to wait. We would welcome volunteers who want to build the Calligra Suite on OSX.
