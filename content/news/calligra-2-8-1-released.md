---
title: Calligra 2.8.1 Released
date: 2014-03-28
author: cyrilleberger
categories: []
---

The Calligra team has released version 2.8.1, the first of the bugfix releases of the [Calligra Suite, and Calligra Active](http://www.calligra.org/) in the 2.8 series. This release contains a few important bug fixes to 2.8.0 and we recommend everybody to update.

## Bugfixes in This Release

Here is an overview of the most important fixes. There are several others that are not mentioned here.

### Kexi

- Bug 332329: do not remove table prefix when it is needed.
- Bug 332293: fix crash on exporting reports as spread sheet document
- Added 'NOT LIKE' SQL operator support in kexi queries.
- Bug 331613: fixes sorting tables in the CSV import dialog.

### Krita

- Fix memory leaks.
- Fix loading and executing macros.
- Save single-layer CMYK images correctly to PSD.
- Bug 331805: do not let the selection grow bigger than the image on invert.
- Bug 329945: fix the Unsharp Mask filter to not be applied with an offset.
- Fix tablet support on MacOS.
- Bug 322022: fix Mirror Mode in Color Smudge and Filter ops.
- Bug 331775: make the Wrap-tool handles less obstructive.
- Bug 332070: do not crash when selecting a template with a stylus doubleclick.
- Bug 331950: mark the document as modified when changing layer properties.
- Bug 331890: fix loading of multilayered PSD files.
- Fix crash in pixellize filter.
- Fix artefacts in the emboss filter.
- Bug 331702: fix crash when saving 16 bits/channel PSD.
- Fix crash in oilpaint filter.

### Flow

- Add stencils icon generation.

### Words

- Do not use calligrawords for plain-text files.

### Try It Out

- **The source code** is available for download: [calligra-2.8.1.tar.xz](http://download.kde.org/stable/calligra-2.8.1/calligra-2.8.1.tar.xz).
- Calligra binaries are available for [download](http://userbase.kde.org/Calligra/Download) for many operating systems.

## About Calligra

Calligra is part of the applications from the KDE community. See more information at the website [http://www.calligra.org/](http://www.calligra.org/).
