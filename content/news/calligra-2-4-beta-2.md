---
title: Calligra 2.4 Beta 2
date: 2011-10-06
author: Inge Wallin
categories: [announcements]
---

The Calligra team is proud and excited to release the second beta version of the [Calligra Suite](http://www.calligra-suite.org/ "Home page of the Calligra Suite"). Since the start of the Calligra effort, we have had a long and very productive development phase. In the mean time we have released four snapshot versions, also known as alphas, to give the users a chance to see what we are doing.

This second beta is the result of the continuous polishing work done by the development team. We are aiming for the final release in November 2011, but already now you can see many bugfixes and improvements. Look in the [full list of changes](http://www.calligra-suite.org/changelogs/calligra-2-4-beta-2-changelog/ "Changelog for Calligra 2.4 beta2") for more details.

News in This Release

The highlights of this release include:

### Productivity Applications

For the productivity part of the suite (word processor, spreadsheet, and presentation program) the target user of version 2.4 is the student or academic user. This version has a number of new features that will make it more suitable for these users.

**Words**, the word processor has improved anchoring and better support for footnotes and endnotes.

**Tables**, the spreadsheet application, has a crash fix and improved compatibility with Microsoft Excel.

**Kexi**, the database application, has many small fixes including improvements in handling of CSV files.

**Plan**, the project management application**,** has a new view category: execution, and improvements to various editor functions.

### Artistic Applications

The artistic applications of the Calligra Suite are the most mature ones and are already used by professional users everywhere.

**Krita**, the drawing application, has a new splashscreen and many bugfixes.

Common Improvements

The architecture of the Calligra Suite lets the applications share much of the functionality of the suite with each other. Many common parts have seen improvements since the last beta release. Here are a few of them. For full details, see again the list of changes.

- Improvements in text layout.
- Improved SVG support
- Speedups of among other things group shapes.

The native file format of Calligra is the Open Document Format, ODF. Compatibility with other suites and formats has always been a priority and as usual all the import filters for Microsoft formats have received attention and improvements. At this time, the Calligra import filters for docx/xlsx/pptx are the best free translations tools available anywhere.

## Try It Out

The source code of the snapshot is available for download: [calligra-2.3.82.tar.bz2](http://download.kde.org/download.php?url=unstable/calligra-2.3.82/calligra-2.3.82.tar.bz2)

### Ubuntu

Users of Ubuntu and Kubuntu are urged to try the daily snapshots prepared by [Project Neon](https://launchpad.net/project-neon). Paste the following in a terminal window and you'll find Calligra installed in /opt:

sudo add-apt-repository ppa:neon/ppa \\
&& sudo apt-get update\\
&& sudo apt-get install project-neon-base \\
   project-neon-calligra \\
   project-neon-calligra-dbg

You can run these packages by adding /opt/project-neon/bin to your PATH.

### Arch Linux

Arch Linux provides Calligra packages in the \[kde-unstable\] repository.

### Fedora

Fedora is working on packages and expects them to be available within the next two weeks.

### OpenSuse

There are OpenSUSE Calligra packages in the [unstable playground repository](https://build.opensuse.org/project/show?project=KDE%3AUnstable%3APlayground).

### Gentoo

Gentoo has Calligra packaged in Portage, tagged as unstable.

### FreeBSD

Calligra ports are available in [Area51](http://freebsd.kde.org/area51.php).

### Windows and OSX

Work has started on a Windows package of the Calligra Application Suite but we are not done yet, so for a Windows build, you will have to wait. We would welcome volunteers who want to build the Calligra Suite on OSX.

## About the Calligra Suite

The Calligra Suite is part of the applications from the KDE community. See more information at the website [http://www.calligra-suite-org/](http://www.calligra-suite.org/).

 

 

The project management application
