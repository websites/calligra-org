---
title: Calligra 2.8 Released
date: 2014-03-05
author: Inge Wallin
categories: []
---

The Calligra team is proud and pleased to announce the release of version 2.8 of the [Calligra Suite](http://www.calligra-suite.org/ "Home page of the Calligra Suite"), Calligra Active and the Calligra Office Engine. This version is the result of thousands of commits which provide new features, polishing of the user experience and bug fixes.

## News in This Release

Calligra 2.8 contains a number of new features. Here is an excerpt of the most important ones.

**Words,** the word processing application, and **Author,** the writer's application, both gained support for comments in the document. \[caption id="attachment\_4068" align="aligncenter" width="385"\][![Adding comments in Calligra Author 2.8](/assets/img/author-2.8-comments-small.png)](http://www.calligra.org/wp-content/uploads/2014/03/author-2.8-comments.png) Adding comments in Calligra Author 2.8\[/caption\]

**Sheets,** the spreadsheet application, has improved support for pivot tables. \[caption id="attachment\_4060" align="aligncenter" width="385"\][![Pivot table in Calligra Sheets 2.8](/assets/img/sheets-pivot-small.png)](http://www.calligra.org/wp-content/uploads/2014/03/sheets-pivot.jpg) Pivot table in Calligra Sheets 2.8\[/caption\]

For **Kexi**, the visual database creator, 2.8 version is focused on improving quality of existing features. Thanks to [close collaboration with users](http://forum.kde.org/kexi), [about 30 issues](http://community.kde.org/Kexi/Releases/Kexi_2.8) has been identified and corrected. The only new feature is ability to open hyperlinks in forms. But the big news is that the app is now available to build and run on Windows for the first time in 2.x series.

\[caption id="attachment\_4056" align="aligncenter" width="380"\][![Query Designer of Kexi 2.8 on Microsoft Windows](/assets/img/kexi-2.8-windows-small.png)](http://www.calligra.org/wp-content/uploads/2014/03/kexi-2.8-windows.png) Query Designer of Kexi 2.8 on Microsoft Windows\[/caption\]

**Flow**, the diagram editor, has new support for SVG based stencils. \[caption id="attachment\_4052" align="aligncenter" width="381"\][![Stencils based on SVG files in Flow 2.8](/assets/img/flow-svg-small.png)](http://www.calligra.org/wp-content/uploads/2014/03/flow-svg.png) Stencils based on SVG files in Flow 2.8\[/caption\]

**Krita,** the full-featured free digital painting studio for artists provides a very exciting release! There are many new features, among them a new high-quality scaling mode for the OpenGL canvas, [G'Mic](http://http://gmic.sourceforge.net/) integration, a wraparound painting mode for easy creation of textures and tiles, support for touch screens.

\[caption id="attachment\_4039" width="160"\][![Switching between touch and desktop mode of Krita Gemini](/assets/img/krita-gemini.jpg)](http://www.youtube.com/watch?feature=player_embedded&v=lLwFJ0jF37s) Switching between touch and desktop mode of Krita Gemini\[/caption\]

\[caption id="attachment\_4036" width="160"\][![Krita Sketch 2.8 running on tablet](/assets/img/krita-tablet-e1394050188366.png)](http://www.calligra.org/wp-content/uploads/2014/03/krita-tablet.jpg) Krita Sketch 2.8 running on tablet\[/caption\]

The 2.8 release also features two new applications based on Krita: **Krita Sketch**, aimed at tablets and **Krita Gemini**, which can switch between desktop and tablet mode at the press of a button. The full set of improvements is huge! Krita has made [its own release announcement on krita.org](http://krita.org/item/220-krita-2-8-0-released), which is well worth looking at. Krita 2.8 will also be available for Windows.

There are also some general **improvements in all apps**. It is now possible to copy and paste any shape between any documents in Calligra applications. Moreover, copying and pasting of images and rich text is now more advanced.  

## Try It Out

![Download small](/assets/img/download-70.png)

The source code of the release is available for download here: [calligra-2.8.0.tar.xz](http://download.kde.org/stable/calligra-2.8.0/calligra-2.8.0.tar.xz). Translations to many languages are [here](http://download.kde.org/stable/calligra-2.8.0/calligra-l10n/) and MD5 sums [here](http://fr2.rpmfind.net/linux/KDE/stable/calligra-2.8.0/MD5SUMS). Alternatively, you can [download binaries for many Linux distributions and for Windows](http://userbase.kde.org/Calligra/Download#Stable_Release).

## About the Calligra Suite

![](/assets/img/calligra-logo-transparent-for-light-600-300x200.png)

The Calligra Suite is part of the applications from [the KDE community](http://www.kde.org/community/whatiskde/). For more information visit [http://www.calligra.org](http://www.calligra.org).
