---
title: "Calligra Suite: the first three months"
date: 2011-03-30
author: boudewijn
categories: []
---

It's been eerily silent around Calligra... I know that. We went public with a bang -- and then nothing apart from regular hints at the commit digest! But that's because everyone is having so much fun hacking! There's lots and lots happening in the calligra git repo, there's a sprint with a record attendence coming up, there are tantalizing hints about Calligra being put to use around the globe...

### Code!

About 3500 commits since we moved to the [Calligra git repo](https://projects.kde.org/projects/calligra/repository)... That's a heck of a lot of work! There are about sixty feature branches, where people develop their cool ideas until they are ready to go into master. This development strategy means that master generally speaking is much more stable than it used to be in the old KOffice subversion days.

I'll skip the cool [Krita](https://projects.kde.org/projects/calligra/repository/revisions/master/show/krita) work for now, since those get described on [Krita's own website](http://www.krita.org), and focus on some of the development highlights for the other apps:

- **Words**: the main thing here is the huge effort by Casper Boemann who, often coding until early in the morning, is revamping the text layout engine. The old text layout engine was probably about the worst thing and really didn't support most of the features users have come to expect of word processors since Word for Windows 2.0. And it wasn't stable: some documents would make the layout engine go berserk, adding pages or continuously moving content about. The new text layout engine is now progressing nicely and already supports things like nested tables, proper drop caps, real unittesting... The layout engine is also encapsulated in a library now. Now when Qt 4.8 comes out and fixes [the font rendering issues](http://bugreports.qt.nokia.com/browse/QTBUG-10615) -- [and there's good hope!](http://labs.qt.nokia.com/2011/03/14/hint-hint-nudge-nudge-say-no-more/) -- then Calligra Words will be really up to scratch!
- **Stage**: Jean-Nicholas Artaud and Benjamin Port have been implementing the outline mode for Stage, something that was still very much missing, and Paul Mendez implemented extended selection for the slide sorter. Stage needs some more work, though, as I discovered when working with Stage for my fosdem presentation. Admirable opportunities to get your hands dirty coding on an application many people would love to use for their presentations at free software conferences all over the world!
- **Tables**: Tables already was pretty good, but that doesn't mean Marijn Kruisselbrink and the other tables developers have been resting on their laurels. Lots and lots of smaller and bigger bugfixes, but also many new and fixed function implementations, faster loading of ODS files and improved compatibility with xls and xlsx files!
- **Karbon**: Karbon is no longer unmaintained: the former Karbon maintainer Jan Hambrecht has returned to the rudder and has implemented some very important features next to smaller fixes like improving the connection shape, renabling wpg support and so on. Karbon (and therefore all of Calligra) now supports ODF glue points and SVG-based shape clipping. Really big features -- now for the first time in Calligra!
- **Kexi**: Adam Pigg has ported Kexi to Maemo and the N900, which means there's now a cool and capable data entry and query application on your mobile phone. And of course, there have been bug fixes, bug fixes and bug fixes.
- **Plan**: formerly known as KPlato, the main improvement here has been the optimizing of the scheduler. At [KO GmbH](http://www.kogmbh.com) we're now using Plan to plan our development projects, and that means that Plan is now really ready for end users!
- **Flow**: after years of not being maintained or developed, Kivio got a new maintainer, Yue Liu. Kivio then got renamed to Flow, to get rid of the boring riff on Microsoft Visio and Yue got busy. The most important missing item, the stencil selector was first on his TODO list, and after that, he started adding useful stencils, and Get Hot New Stuff integration so users can share stencils.
- **Braindump**: this is a completely new application that allows users to place notes and ideas on a canvas. It's young, but very promisting.
- **Calligra Mobile**: the main thing here is the work by Jaroslaw Staniek to abstract the underlying code in the koabstraction library, which makes it really easy to create qwidget-based office-document enabled applications in only a few lines of code.

And of course, the ongoing work to improve our import filters, fix bugs, optimize the libraries and so on didn't flag. Soon we'll have the first snapshot release of Calligra Master, which will make it possible to test and give feedback on all the improvements and work that has gone into Calligra. We intend to continue releasing snapshots until at least Calligra Words is ready for end users. That means the new text layout engine must have landed, and the gui for selecting styles will have been rewritten.

### Sprint!

This weekend a large number of Calligra hackers from all over the world will come to Berlin to meet up. Many of them for the first time, others were out of development for some time during the last KOffice days, but are now happily hacking again. Over thirty attendees... It's getting to be a sort of mini conference, as Sven Langkamp said!

We'll be discussing the new release cycle, smoothing out the loading of plugins, fixing the UI mistakes we made in the early KOffice days, the balance between depending on Qt and on KDE, more separation between UI and engine. Hard, heavy duty topics, and we only have a couple of days!

And there will be workshops, on the new text layout engine, marketing, qgraphicsview/qml integration and more.

### Calligra everywhere

At Fosdem 2011, I gave [a main track presentation](http://www.valdyas.org/~boud/calligra_under_the_hood_fosdem_2011.pdf) on how Calligra can be used as a flexible component in many kinds of applications to provide support for office documents. Ranging from report writers to custom tablet gui's, Calligra can go pretty much anywhere where there's Qt. Many of these projects are still under wraps, others are being developed out in the open, like Calligra Mobile, formerly FreOffice. Jaroslaw Staniek made a nice abstraction on top of Calligra Mobile, which makes it really easy to create new QWidget-based calligra applications. And I and Inge demonstrated a prototype of a QML based tablet gui for Calligra at the Mobile World congress.
