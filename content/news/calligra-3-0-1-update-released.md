---
title: Calligra 3.0.1 update released
date: 2017-04-17
author: boemann
categories: []
---

We have released a bugfix update of Calligra. The 3.0.1 version contains the following fixes:

General

- Fix crash in move command when using arrow keys
- Respect container boundaries when moving shapes by arrow keys
- Remove shape manipulation handles when the tool is deactivated
- Always display shapes in the the same order in the 'Add shape' docker

Sheets

- Improve formatting of scientific numbers
- Fix save/load of cell borders

Plan

- Bug 376469: Bad month calendar in Work & Vacation
- Day numbers where not initialized correctly.
- Manually entered dates where not parsed correctly.
- Use default currency symbol if the currency symbol is not explicitly set

Chart

- Fix crash when chart type is changed
- Fix crash when a chart component is deleted
- Fix crash when x- or y-axis is removed
- Fix ui when editing axis label
- Limit moving chart components to chart boundaries
- Fix edit font dialog: Keep the axis fonts QFont size in sync with KCharts fontSize
- Fix save/load of axis label font size and font family
- Save/load legend alignment flags
- Do not save axis label if it is not visible
- Always do legend alignment when legend becomes visible.
- Make axis dimensions translatable
- Add undo command for hide/show titles
- Add undo command for add/remove axis
- Respect margins/spacing
- Handle resizing in a reasonable way
