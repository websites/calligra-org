---
title: Calligra 2.8.2 Released
date: 2014-04-16
author: jstaniek
categories: []
---

The Calligra team has released a bugfix version 2.8.2 of the [Calligra Suite, and Calligra Active](http://www.calligra.org/). This release contains a few important bug fixes to 2.8.0 and we recommend everybody to update.

## Bugfixes in This Release

Here is an overview of the most important fixes. There are several others that are not mentioned here.

### General

- Move Export to PDF command to the Export menu section instead of the Print section for conformance with other office suites (bug 332603).
- Fix "Missing import filter" bug when saving if not extension is specified (even if selecting a File type is set to a format) (bug 328975).
- Use native file dialogs on Windows.

### Kexi

- Make _Rich Text_ property false by default for _Text Editor_ form widget. Rich Text often [causes misbehaviour](http://forum.kde.org/viewtopic.php?f=221&t=120309).

### Krita

- Fix resetting the slider spin box when double clicking on it (bug 330165).
- Ignore tablet press/release events which did not produce any sane buttons (bug 331925).
- Added support for 'evdev' tablets (bugs 332239, 331572, 329641).
- Save line smoothing options between runs of Krita. This is really needed for low-level tablets like Genius to filter the trajectory they report to us.
- Make Krita auto-recognize axes labels of Evdev tablets. The labels are stored in a special property of the XInput device.
- Recognize Surface Pro 2 tablets on Windows (bug 331922).
- Fixed size of predefined images.
- Set default gradient to alpha (bug 329008).
- Clean up the layout of the Transform tool.
- Hide unused settings for stroke in the Path tool (bug 331556).
- Fixed memory leaks in brush handling.
- Fixed memory leaks when resources fail to load.
- Fixed memory leaks when creating strokes (bug 331592).
- Don't crash on creating a file layer in Krita Gemini (bug 332871).
- Improved splash screen.
- Fix loading plugins for Krita Gemini.
- Save tags with special characters properly (bug 332708).
- Fix removing of tags, don't load or save dummy tags.
- Add import for PSD layer groups (bug 289857).
- Fix translation issues.
- Fix startup of Krtita Sketch and Gemini.

### Try It Out

- **The source code** is available for download: [calligra-2.8.2.tar.xz](http://download.kde.org/stable/calligra-2.8.2/calligra-2.8.2.tar.xz).
- Calligra binaries are available for [download](http://userbase.kde.org/Calligra/Download) for many operating systems.

## About Calligra

Calligra is part of the applications from the KDE community. See more information at the website [http://www.calligra.org/](http://www.calligra.org/).
