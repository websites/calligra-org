---
title: Calligra 2.5 Released
date: 2012-08-13
author: Inge Wallin
categories: []
---

The Calligra team is proud and pleased to announce version 2.5 of Calligra. This is the second stable release of the [Calligra Suite](http://www.calligra-suite.org/ "Home page of the Calligra Suite") and Calligra Active, the mobile variation of Calligra.

\[caption id="attachment\_3379" align="aligncenter" width="500"\][![Tight run-around in Words](/assets/img/our-planet-earth_screenshot-500x285.png)](http://www.calligra.org/news/calligra-2-5-alpha-released/attachment/our-planet-earth_screenshot/) Tight run-around in Words\[/caption\]

## News in This Release

This release has a number of new or improved features. The most important of them will be highlighted here.

### Productivity Applications

For the productivity part of the suite (word processor, spreadsheet, and presentation program) the target user of version 2.5 is still the student or academic user. This version has a number of new features that will make it more suitable for these users.

**Words**, the word processor, has [among other things](http://community.kde.org/Calligra/Schedules/2.5/Feature_Plan#Words) improved support for [editing of tables](http://player.vimeo.com/video/40126803), tight run-around of text around images, manipulation of table borders, and dragging of text. There are also a number of new features in the bibliography tools.

**Sheets**, the spreadsheet application has a new stand-alone docker for the cell editor. In Version 2.4 of Calligra Sheets, the cell editor was a standard tool docker, which meant that most people kept it in the side bar. Some users thought this was a disadvantage because the editor area was too narrow. In version 2.5 the cell editor is now contained in its own dock window which means that it can be placed above the work area similar to other spreadsheet applications, or left or right or even torn off into its own window.

**Sheets** also has a new cell tool window with cell formatting controls. In version 2.4 Sheets used mostly menus and dialogs for formatting cells. In version 2.5 the most common formatting options are collected into a cell tool option window much like the text formatting controls in Words. \[caption id="attachment\_3415" align="aligncenter" width="500"\][![New user interface in Calligra Sheets 2.5](/assets/img/sheets_new_ui-500x297.png)](http://www.calligra.org/news/calligra-2-5-beta-released/attachment/sheets_new_ui/) New user interface in Calligra Sheets 2.5\[/caption\]

**Stage**, the presentation program, has a number of usability improvements.

**Flow**, the diagram application, has support for new stencils in odf custom shapes. The users will also take advantage of the new shape connectors described below.

**Kexi**, the database application, now offers a full screen mode activated by the F11 key. New form elements Command Link Button, Slider, Progress Bar and Date Picker widgets have been added to Forms. Full list of changes in Kexi is available on [community.kde.org](http://community.kde.org/Kexi/Releases/Kexi_2.5#List_of_changes).

\[caption id="attachment\_3426" align="aligncenter" width="419"\][![New form widgets in Kexi 2.5](/assets/img/kexi-2.5-new-form-widgets-419x400.png)](http://www.calligra.org/news/calligra-2-5-beta-released/attachment/kexi-2-5-new-form-widgets/) New form widgets in Kexi 2.5\[/caption\]

**Braindump**, the note taking application, has no new features in itself but will take advantage of all the improvements in shared shapes and plugins.

### Artistic Applications

The artistic applications of the Calligra Suite are the most mature ones and are already used by professional users everywhere.

**Krita**, the painting application, has a [new compositions docker](http://slangkamp.wordpress.com/2012/05/09/krita-compositions-docker/), useful in movie storyboard generation. Other new features are textured painting and many performance improvements.

**Krita** also has an improved canvas handling subsystem. This means that rotation and scaling of the canvas can now be done when any other tool is active. This should improve the interaction for artists that make much use of the canvas manipulation feature.

## Filters

There are three new filters in Calligra 2.5:

- A new import filter for **Visio** files.
- A new import filter for **MS Works**.
- A new import filter for **xfig** files.

And of course all the filters for **MS Office** have been improved even more, especially for the OOXML formats used in MS Office 2007 and 2010.

## Common Improvements

The architecture of the Calligra Suite lets the applications share much of the functionality of the suite with each other. Many common parts have seen improvements since the release of 2.4. Here are a few of them.

The **management of autosave files** has been much improved.

There is a new system for **managing user profiles**. The user can pick which profile (personal, work, etc) to use when creating and saving a document. There are two predefined profiles: anonymous and one created from the system info.

There is a **new system for connecting shapes**. This is used for instance when creating diagrams for flowcharts or organizational charts. The new connectors in Calligra 2.5 are both more powerful and easier to use than those in version 2.4.

**Images now support various new effects** such as gray and black/white image effects. There is also new support for mirroring images on any axis.

**Charts** have a number of improvements and work much better in general.

The common parts of Calligra was the subject of a week-long bug fixing sprint where over 100 bugs was closed. This means that the overall quality of the shared plugins is improved.

## Other Than the Desktop

At the same time as the desktop version, the community also releases a QML based version for tablets and smartphone: **Calligra Active**. The Calligra team has decided to focus on Calligra Active as the main mobile version for the future.

## Try It Out

The source code of the snapshot is available for download: [http://download.kde.org/stable/calligra-2.5.0/calligra-2.5.0.tar.bz2](http://download.kde.org/stable/calligra-2.5.0/calligra-2.5.0.tar.bz2)

Many Linux distributions will package Calligra for their users. We will keep [this page](http://userbase.kde.org/Calligra/Download) updated with the latest details.

### Windows and OSX

Windows packages will be available from [KO GmbH](http://www.kogmbh.com/). For download of Windows binaries, use the [download page](http://www.kogmbh.com/download.html). We would welcome volunteers who want to build and publish packages of the Calligra Suite for OSX.

## About the Calligra Suite

The Calligra Suite is part of the applications from the KDE community. See more information at the website [http://www.calligra.org/](http://www.calligra.org/).
