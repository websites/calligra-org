---
title: Calligra 2.7 Beta Released
date: 2013-05-08
author: Inge Wallin
categories: []
---

The Calligra team is proud and pleased to announce the beta release of version 2.7 of the [Calligra Suite](http://www.calligra-suite.org/ "Home page of the Calligra Suite"). This means that the calligra/2.7 branch has been created and from now on Calligra 2.7 will only see bugfixes but no new features. The final release of 2.7 is planned in approximately a month from now..

## News in This Release

Calligra 2.7 contains a number of new features. Here is an excerpt of the most important ones, the full list will be available at the release of 2.7.0.

**Words,** the word processing application, has a new look for the toolbox. In the same toolbox there are also new controls to manipulate shapes with much enhanced usability.  Styles are now sorted with the used ones first in the style chooser which should make using styles much more convenient. There are also enhancements to the reference controls, especially regarding hyperlinks and bookmarks.

**Author,** the writer's application, has new support for EPUB3: mathematical formulas and multimedia contents are now exported to ebooks using the EPUB format.  There is also new support for book covers using images.

There is a new export filter for common text and enhancements to the docx import filter, which can now support predefined table styles. Note that all of these enhancements to Words and Author are also available in the other application. We note here where they were originally developed.

**Sheets,** the spreadsheet application, has new support for pivot tables. This is a Google Summer of Code project that was integrated into the Calligra code base during the 2.7 development cycle.

**Plan,** the project management application, has improvement in the scheduling of tasks and new export filters to OpenDocument Spreadsheet (ODS) and CSV.

In **Kexi**, the visual database creator, there is a new feature to import CSV data into an existing table.

There are some improvements in the general **shapes** available from most Calligra applications: The formula shape now has new ways to enter formula: a matlab/octave mode and a LaTEX mode. The tool for the text shape is improved to better handle text styles.

Another change common to Words, Author and Stage is that the visual style editor got improvements.

**Krita,** the 2D paint application, has new grayscale masks, a better freehand path tool with line smoothing, a way to paint shapes with Krita brushes, improved color to alpha filter, an improved crop tool and an improved transform tool that helps the user create textures. Support for new file formats include export to QML and a much improved import/export filter for photoshop PSD files.

Unfortunately there were two large features that didn't make the deadline: Annotations in Words/Author and export to the Microsoft Docx format. These features will be present in the next version of Calligra.

## Try It Out

The source code of the snapshot is available for download: [calligra-2.6.90.tar.bz2](http://download.kde.org/unstable/calligra-2.6.90/calligra-2.6.90.tar.bz2). Alternatively, you can [download binaries for many Linux distributions and windows](http://userbase.kde.org/Calligra/Download#Unstable_Release).

## About the Calligra Suite

The Calligra Suite is part of the applications from the KDE community. See more information at the website [http://www.calligra.org/](http://www.calligra.org/).
