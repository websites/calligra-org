---
title: Calligra 2.5.3 Released
date: 2012-11-06
author: Inge Wallin
categories: []
---

The Calligra team has released version 2.5.3, the third bugfix release of the [Calligra Suite,  and Calligra Active](http://www.calligra.org/). This release contains a number of important bug fixes to 2.5.2 and we recommend everybody to update as soon as possible.

## Bugfixes in This Release

Here is an overview of the most important fixes that are in 2.5.3. There are several others that are not mentioned here.

### Kexi:

- Force lower case letters for object identifiers (except for widget names) (bug 306523)
- Database Support Library: Keep table names in original format but compare them in case-insensitive way when needed. This way importing from databases with tables upper-case characters works.
- SQLite Driver: Fix possible data loss of compacted file when tools crash or fail for any reason.
- Database Migration: Fix invalid SQL generation for database import.
- Database Migration: Fix importing from databases when table names contain upper-case characters (bug 307479)
- Main Window: Fix crashes on Kexi closing (bug 299484)
- Main Window: display names in tab bars, not captions (for consistency); names are displayed in the Project Navigator too.
- Table Designer: Fix possible crash when saving changes to design of table used elsewhere (bug 306672)
- Query Designer: Fix: Missing "\*" (All Columns) item in Query Designer; it was present in Kexi 2.2 and ealier (bug 306577)
- Force lower case letters for object identifiers (bug 306523)

Detailed list of changes for Kexi is [available](http://community.kde.org/Kexi/Releases/Kexi_2.5.3).

## Try It Out

The source code of this version is available for download here: [calligra-2.5.3.tar.bz2](http://master.kde.org/stable/calligra-2.5.3/calligra-2.5.3.tar.bz2). Also many [binary packages](http://userbase.kde.org/Calligra/Download) are available for various operating systems and distributions. This release is packaged for Windows by [KO GmbH,](http://www.kogmbh.com/ "KO GmbH homepage") and you can find it on their [download page](http://www.kogmbh.com/download.html). There is also a separate package or [Krita](http://www.krita.org/) only.

We would welcome volunteers who want to build and test the Calligra Suite on OSX.

## About Calligra

Calligra is part of the applications from the KDE community. See more information at the website [http://www.calligra.org/](http://www.calligra.org/).
