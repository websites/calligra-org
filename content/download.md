---
title: Download
layout: download
appstream: org.kde.calligra
name: Calligra
gear: false
flatpak: true
menu:
  main:
    weight: 2
sources:
  - name: Git
    src_icon: /reusable-assets/git.svg
    description: >
        Calligra is spread across a multitude of repositories. Check our
        [Getting Involved](/get-involved) page for detailed
        instructions how to get all Kontact repositories.
  - name: Windows
    src_icon: /reusable-assets/windows.svg
    description: >
        We are currently working on bringing Calligra to Windows. Do you want
        to help us? Check [how to get involved](/get-involved)
        and get in touch with us!
konqi: /assets/img/konqi-mail.png
components: true
---
